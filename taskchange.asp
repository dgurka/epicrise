<!--#include file="header.asp" -->
<!--#include file="freeaspupload.asp" -->
<%if not(user_rights(36)) then server.transfer "defaultpage.asp"
task_ID=cleanInput(request("task_ID"))

if(request.querystring("upload")<>"") then
end if

query="Select * from task where task_ID="&task_ID
rs.open query, conn

task_summary = rs("summary")
task_date_due = rs("date_due")
task_date_assigned = rs("date_assigned")
notes_ID = rs("notes_ID")

rs.close

query="Select * from notes where notes_ID="&notes_ID
rs.open query, conn

note_note = rs("note")

rs.close

'test tasks

%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Move Task</title>
<style>
	table.notestable {
		background-color: black;
	}
	table.notestable tr {
		background-color: white;
	}
	table.notestable td {
		vertical-align: top;
		padding: 10px;
	}
</style>

<script type="text/javascript">
function editnote(note) {
	notesbox=document.getElementById("notesbox"+note);
	notes=document.getElementById("notes"+note).innerHTML;
	noteseditbox=document.getElementById("noteseditbox"+note);
	edittext=document.getElementById("noteseditform"+note).innerHTML;
	notesbox.innerHTML=edittext;//'<form action="notes.asp" method="POST"><input type="hidden" name="action" value="edit"><input type="hidden" name="city" value="<%=city%>" /><input type="hidden" name="notes_ID" value="'+note+'" /><input type="hidden" name="prevlocation" value="<%=prevlocation%>" /><textarea name="note" style="width:100%;height:200px;">'+notes+'</textarea><input type="submit" value="Update" /></form>';
}

function deletenote(note) {
	if (confirm("Are you sure you want to delete this note?\n\nDeleting a note is permanent!")) {
		note.submit();
	}
	return;
}

function page(pagenum)
{
	document.page.page.value=pagenum;
	document.page.submit();
}

function validatenumber(elem) {
var myval;
myval=Math.abs(elem.value);
if (isNaN(myval)) {
myval=0;
} else {
myval=myval*4;
myval=Math.ceil(myval);
myval=myval/4;
}
myval=myval.toFixed(2);
elem.value=myval;
}

document.onclick=function(event){docclick(event)};
var allowclose=true;
function docclick(event)
{
	if (allowclose)
	{
		var ev = window.event ? window.event : event;
		var obj = ev.target ? ev.target : ev.srcElement;
		while (obj)
		{
			if (obj.getAttribute)
				if ((obj.getAttribute("name")=="calendar_display") | (obj.getAttribute("ID")=="date_work"))
					return;
			obj=obj.parentNode;
		}
		var cals = document.getElementsByTagName("div");
		var i = 0;
		while (i<cals.length)
		{
			if (cals[i].getAttribute("name")=="calendar_display")
				cals[i].style.display='none';
			i++;
		} 
	}
}

var xmlHttp;
var calendarEditBox;
var calendarDisplayBox;

function calendarShow(thebox, note_ID)
{
if (calendarDisplayBox) calendarClose();
calendarEditBox = thebox;
calendarDisplayBox = document.getElementById("calendar" + note_ID);
calendarDisplayBox.style.display="block";
calendarEditBox.blur();
calendarDisplayBox.focus();
allowclose=false;
setTimeout('allowclose=true;', 100);
}

function calendarClose()
{
calendarDisplayBox.style.display="none";
}

function calendarSelect(month, day, year)
{
calendarDisplayBox.style.display="none";
calendarEditBox.value=month+"/"+day+"/"+year;
calendarMove(calendarEditBox.value);
}

function calendarUpdate()
{
if (xmlHttp.readyState==4)
{
calendarDisplayBox.innerHTML=xmlHttp.responseText;
}
}

function calendarMove(newdate)
{
xmlHttp=createXmlHttp();
xmlHttp.onreadystatechange=calendarUpdate;
xmlHttp.open("GET","calendar.asp?curdate="+escape(newdate),true);
xmlHttp.send(null);
}

function task_summary_focus()
{
	var blankitem = document.addnoteform.task_summary_blank;
	var summary = document.addnoteform.task_summary;
	if (blankitem.value=='1') {
		blankitem.value='0';
		summary.value='';
		summary.style.color='black';
	}
}

function task_summary_blur()
{
	var blankitem = document.addnoteform.task_summary_blank;
	var summary = document.addnoteform.task_summary;
	if (summary.value=='') {
		blankitem.value='1';
		summary.value='Summary';
		summary.style.color='gray';
	}
}

function validate_form()
{
	if (document.addnoteform.task_summary_blank.value=='0') {
		if (document.addnoteform.task_users_ID.value=='-1') {
			alert('You entered a task but did not select a user');
			return false;
		}
	}
	return true;
}

var attachment_num = 1;

function add_attachment()
{
	var my_div = document.getElementById('attachments');
	el = document.createElement("input");
	el = my_div.appendChild(el);
	el.name = "attachment_name_"+attachment_num;
	el.type = "text";
	el.style.width="100px";
	
	el2 = document.createElement("input");
	el2.type = "file";
	el2.name = "path"+attachment_num;
	el2 = my_div.appendChild(el2);
	
	my_div.appendChild(document.createElement("br"));
	
	attachment_num = attachment_num + 1;
}

function disablebutton(btn) {
	btn.disabled=true;
	btn.value="Uploading...";
	document.addnoteform.submit();
}

<%if (user_rights(36)) then%>
var tasktimer;
var taskhidetimer;

function mouseovertask(task)
{
	tasktimer = setTimeout('showedittask('+task+');',500);
}

function mouseouttask(task)
{
	clearTimeout(tasktimer);
	taskhidetimer = setTimeout('hideedittask('+task+');',5000);
}

function showedittask(task)
{
	document.getElementById('edit_task_'+task).style.display='block';
}

function hideedittask(task)
{
	document.getElementById('edit_task_'+task).style.display='none';
}<%end if%><%if (user_rights(39)) then%>

function approvenote(notes_ID)
{
	document.approveform.notes_ID.value = notes_ID;
	document.cookie = "scrollLeft=" + document.body.scrollLeft + ";scrollTop=" + document.body.scrollTop;
	document.approveform.submit();
}<%end if%>

function init()
{
	<%
	if (request.QueryString("note")<>"") then
		note_ID=request.querystring("note")
	%>	shownote(<%=note_ID%>);
<%end if%>
	<%
	if (request.form("action")="approve") then%>
	shownote(<%=request.form("notes_ID")%>);
	<%end if%>
}

function shownote(note)
{
	var noteboxes = document.getElementsByTagName('table');
	var cells;
	x=0;
	do
	{
		if (noteboxes[x].id.substring(0,7)=="notebox")
		{
			noteboxes[x].className = noteboxes[x].className.replace(/selected/g, "");
		}
		x++;
	} while (x<noteboxes.length);
	
	var notebox = document.getElementById('notebox_'+note)
	if (notebox)
	{
		<%if (request.form("action")<>"approve") then%>
		notebox.className += " selected";
		<%end if%>
		window.location='#note_'+note;
	} else {
		window.location="notes.asp?city=<%=job_ID%>&note=" + note + "#note_" + note;
	}
}

var pageTitle = "<%=Replace(city, """", "\""")%><%if (state<>"") then%>, <%=Replace(state, """", "\""")%><%end if%><%=chr(32)%><%=Replace(name, """", "\""")%>";

function printnote(note) {
	shownote(note);
	window.print();
}
</script>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<h3><%= task_summary %></h3>

<form action="taskchange.asp?task_ID=<%= task_ID %>&upload=1" method="POST" name="addnoteform" enctype="multipart/form-data" onsubmit="return validate_form();">
<input type="hidden" name="task" value="<%=task_ID%>" />
<input type="hidden" name="action" value="post" />
<%if (user_rights(23)) or (user_rights(20)) or (user_rights(34)) then%><table style="width: 900px;"><tr><%if (user_rights(23)) then%><td><table><tr><td>Hours</td><td> <input type="text" name="hours" onchange="validatenumber(this);" /></td></tr>
<tr><td>Billable:</td><td><select name="billable">
<%query="Select * from billable order by rank ASC"
rs.open query,conn
do until rs.eof
%><option value="<%=rs("billable_ID")%>"><%=rs("name")%></option>
<%
rs.movenext
loop
rs.close
%></select></td></tr>
<tr><td>Work Date</td><td><div style="display:inline;position:relative;"><input type="text" size="8" name="date_work" value="<%if (edit) then response.write date_work else response.write FormatDateTime(LocalizeDate(Date), vbShortDate)%>" onfocus="calendarShow(this, '');" readonly />&nbsp;<div id="calendar" name="calendar_display" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%if (edit) then curdate=date_work else curdate=Date%><!--#include file="calendar.asp" --></div></div></td></tr>
<tr>
	<td>New Status</td>
	<td>
		<select name="task_status" id="task_status">
			<option value="1">New</option>
			<option value="2">Do</option>
			<option value="3">Test</option>
			<option value="4">Invoice</option>
			<option value="5">Completed</option>
		</select>
	</td>
</tr>
<tr align="left">
	<td>Assign User</td>
	<td><select name="task_users_ID"><option value="-1">Select a user</option>
<% query="Select users.users_ID, users.full_name from users inner join user_level_rights on users.user_level=user_level_rights.user_level_ID WHERE active = 1 AND user_level_rights.user_rights_ID=33"
rs.open query, conn
do until rs.eof
%>  <option value="<%=rs("users_ID")%>"><%=rs("full_name")%></option>
<%rs.movenext
loop
rs.close %>
</select></td>
</tr>
</table></td><%end if%><%if ((user_rights(20)) or (user_rights(27))) then%><td align="center" valign="bottom" style="padding-bottom:5px;">
<table width="100%" height="100%"></table></td>
<%end if%></tr>
</table><%
else
%>
<%end if%>
<textarea name="note" style="width:900px; height:200px;"></textarea><br/>
<div id="attachments">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File<br/></div>
<input type="button" value="Post" onclick="disablebutton(this);" />&nbsp;<input type="button" onclick="add_attachment();" name="addattachment" value="Add Attachment" />
<input type="hidden" name="grouptasks" value="<%if (grouptasks) then %>yes<%else%>no<%end if%>" />
</form>
<br/><a href="task.asp?task_ID=<%= task_ID %>">Go to task</a>

<!--#include file="bodyclose.asp" --></body></html>