<!--#include file="header.asp" --><%
if not(user_rights(31)) then response.redirect "defaultpage.asp"

Function encodeString(input)
 Dim newStr : newStr = ""
 for i = 1 to len(input)
  newStr = newStr & chr((asc(mid(input,i,1))+8))
 next
 encodeString = newStr
End Function

Function decodeString(input)
 Dim oldStr : oldStr = ""
 for i = 1 to len(input)
  oldStr = oldStr & chr((asc(mid(input,i,1))-8))
 next
 decodeString = oldStr
End Function

if Request.ServerVariables("REQUEST_METHOD")= "POST" then
	query = "INSERT INTO credentials (type, uname, pname, client_id) VALUES ('" & request.Form("type") & "', '" & encodeString(request.Form("u")) & "', '" & encodeString(request.Form("p")) & "', '" & request.Form("clid") & "')"
	rs.open query, conn
end if

%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Info</title>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->

<%
'Request.Cookies("EPICRISE_user")

query = "SELECT u.users_ID, c.client_id, * FROM (credentials AS c INNER JOIN credentials_users AS cu ON c.client_id = cu.client_id) INNER JOIN users AS u ON cu.users_id = u.users_ID WHERE (((u.users_ID)="& Request.Cookies("EPICRISE_user") &") AND ((c.client_id)="& request.QueryString("clid") &"));"
'Response.Write(query)
rs.open query, conn

%>

<table>
<tr>
	<th>Type</th>
	<th>U</th>
	<th>P</th>
	<th>Remove</th>
</tr>
<%
	do until rs.eof
		%>
		<tr>
			<td><%= rs("type") %></td>
			<td><%= decodeString(rs("uname")) %></td>
			<td><%= decodeString(rs("pname")) %></td>
			<td><a href="delete_cred.asp?id=<%= rs("ID") %>&clid=<%= request.QueryString("clid") %>">Remove</a></td>
		</tr>
		<%
		rs.MoveNext
	loop
	rs.close
%>
</table>

<h4>Add new credential</h4>
<form name="addcreds" method="post">
    <table>
        <tr>
            <td>Type: </td>
            <td><input type="text" name="type" /></td>
        </tr>
        <tr>
            <td>U: </td>
            <td><input type="text" name="u" /></td>
        </tr>
        <tr>
            <td>P: </td>
            <td><input type="text" name="p" /></td>
        </tr>
    </table>
	<input type="hidden" name="clid" value="<%= request.QueryString("clid") %>" />
	<input type="submit" value="Save Credential" />
</form>

<% if user_rights(30) then %>
    <h3>Permissions</h3>
    <h4>Current Users</h4>
    <%
	query = "SELECT * FROM credentials_users INNER JOIN users ON credentials_users.users_id = users.users_ID WHERE (((credentials_users.client_id)="& request.QueryString("clid") &"));"
	rs.open query, conn
		Dim MyList(1)
		do until rs.eof
			Response.Write(rs("name") & "<br/>")
			rs.MoveNext
		loop
	rs.close
	%>
    <h4>Options</h4>
    <form action="edit_perm.asp" method="post">
    <%
    query = "SELECT * FROM users WHERE active=1"
    rs.open query, conn
    do until rs.eof 
		%>
		<input type="checkbox" name="check<%= rs("users_ID") %>" value="checked" />
		<span><%= rs("name") %></span><br/> 
		<% rs.MoveNext
    	loop %>
    <input type="hidden" name="clid" value="<%= request.QueryString("clid") %>" /><br/>
    <input type="submit" value="Save Permissions" />
    </form>
<% end if %>

<!--#include file="bodyclose.asp" --></body></html>