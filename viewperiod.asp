<!--#include file="header.asp" --><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>View Period</title>
<script src="xmlHttp.js"></script>
<style>
table {
background-color:#000000;
width:800px;
}
td {
background-color:#FFFFFF;
padding: 4px;
}
</style>
<script type="text/javascript">
var xmlHttp;
function edit_timesheet(timesheet)
{
document.getElementById("time"+timesheet).innerHTML='<input type="hidden" name="timesheet" value="'+timesheet+'" /><input type="hidden" name="period" value="<%if (request.form("curdate")="") then response.write request.querystring("curdate") else response.write request.form("curdate")%>" /><input type="hidden" name="user" value="<%=request.form("user")%>" /><input name="time" size="6" value="'+document.getElementById("time"+timesheet).innerHTML+'" />';
document.getElementById("info"+timesheet).innerHTML='<textarea name="details" cols=70 rows=25>'+document.getElementById("info"+timesheet).innerHTML.replace(/<br>/gi,'\n')+'</textarea><br><input type="submit" value="Save" />';
document.getElementById("edit"+timesheet).style.display="none";
document.getElementById("editinfo"+timesheet).style.display="none";
}
function delete_timesheet(timesheet)
{
	if (confirm("Are you sure you wish to remove this entry?")) {
		document.deleteform.timesheet.value=timesheet;
		document.deleteform.submit();
	}
}<%if (user_rights(39)) then%>

function approvenote(notes_ID)
{
	document.approveform.notes_ID.value = notes_ID;
	document.approveform.submit();
}<%end if%>
</script></head>
<%
timesheet_user_ID=Request.Cookies("EPICRISE_user")
timesheet_offset=0
if (user_rights(24)) then
if (request.form("user")<>"") then
timesheet_user_ID=request.form("user")
end if
end if
if (request.form("curdate")="") then
curdate=request.querystring("curdate")
else
curdate=request.form("curdate")
end if
if (day(curdate)>15) then
curdate=DateAdd("d",16-day(curdate),curdate)
else
curdate=DateAdd("d",1-day(curdate),curdate)
end if
if (day(curdate)=1) then
enddate=DateAdd("d",15,curdate)
else
enddate=DateAdd("d",-15,curdate)
enddate=DateAdd("m",1,enddate)
end if
curtimedate=date()
if (day(curtimedate)>15) then
curtimedate=DateAdd("d",16-day(curtimedate),curtimedate)
else
curtimedate=DateAdd("d",1-day(curtimedate),curtimedate)
end if
query="Select * from users where users_ID="&timesheet_user_ID
rs.open query, conn
fullname=rs("full_name")
rs.close

if (request.form("action")="approve") and (user_rights(39)) then
	if (isNumeric(request.form("notes_ID"))) then
		query="UPDATE notes SET approved_ID=2 WHERE notes_ID=" & request.form("notes_ID")
		rs.open query, conn
	end if
end if
%>
<body><!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<form name="approveform" action="viewperiod.asp" method="POST" style="display:none;">
<input type="hidden" name="curdate" value="<%=request.form("curdate")%>"/>
<input type="hidden" name="showzero" value="<%=request.form("showzero")%>">
<input type="hidden" name="user" value="<%=request.form("user")%>">
<input type="hidden" name="notes_ID" value="" />
<input type="hidden" name="action" value="approve" />
</form>
<div style="font-size:18px;"><%=fullname%>'s Timesheet for <%=curdate%> to <%=enddate%>&nbsp;<form name="showzeroform" action="viewperiod.asp" method="POST" style="display:inline;"><input type="hidden" name="curdate" value="<%=request.form("curdate")%>"/><input type="hidden" name="user" value="<%=request.form("user")%>"><input type="checkbox" name="showzero" value="yes" <%if (request.form("showzero")="yes") then %>checked <%end if%>onclick="document.showzeroform.submit();" />Show Zero Hours</form></div>
<div style="font-size:5px;"><br></div>
<div style="display:none;"><form name="deleteform" action="edittime.asp" method="POST"><input type="hidden" name="timesheet" value="" /><input type="hidden" name="action" value="delete" /><input type="hidden" name="period" value="<%if (request.form("curdate")="") then response.write request.querystring("curdate") else response.write request.form("curdate")%>" /></form></div>
<table><tr style="font-weight:bold;"><td style="width:100px;">Date</td><td style="width:100px;">Total Hours</td><td style="width:100px;">Customer</td><td style="width:100px;">Job</td><td width="500">Details</td></tr>
<%
if (request.form("showzero")="yes") then
query = "Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes left outer join task on notes.notes_ID=-task.task_ID WHERE notes.date_work>=#"&curdate&"# and notes.date_work<#"&enddate&"# and notes.users_ID="&timesheet_user_ID& _ 
vbcrlf & " UNION ALL " & vbcrlf & _
"Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes right outer join task on notes.notes_ID=task.notes_ID WHERE task.date_completed>=#"&curdate&"# and task.date_completed<#"&enddate&"# and task.users_ID="&timesheet_user_ID&" ORDER BY datesort DESC"
else
query = "Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes left outer join task on notes.notes_ID=-task.task_ID WHERE notes.date_work>=#"&curdate&"# and notes.date_work<#"&enddate&"# and notes.users_ID="&timesheet_user_ID&" and hours<>0"& _ 
vbcrlf & " UNION ALL " & vbcrlf & _
"Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes right outer join task on notes.notes_ID=task.notes_ID WHERE task.date_completed>=#"&curdate&"# and task.date_completed<#"&enddate&"# and task.users_ID="&timesheet_user_ID&" ORDER BY datesort DESC"
end if
rs.open query,conn
do until rs.eof
note_ID=rs("notes.notes_ID")
query="Select * from job WHERE job_ID="&rs("job_ID")
rs2.open query, conn
job_ID=rs2("job_ID")
job_name=rs2("name")
if (isNull(rs("approved_ID"))) then approved_ID="" else approved_ID=rs("approved_ID")
query="Select * from client where client_ID="&rs2("client_ID")
rs2.close
rs2.open query,conn
customer_name=rs2("name")
if (rs2("state")<>"") and not(isNull(rs2("state"))) then customer_name = customer_name & ", " & UCase(rs2("state"))
rs2.close
if not(isNull(rs("notes.editor_ID"))) then
query="Select * from users where `users_ID`="&rs("notes.editor_ID")
rs2.open query,conn
editor_name=rs2("name")
rs2.close
else
editor_name=""
end if
query="Select * from billable where billable_ID="&rs("billable_ID")
rs2.open query,conn
if (rs2.eof) then
billable=UNDEFINED_VAL
else
billable=rs2("name")
end if
rs2.close
if (approved_ID<>"") then
	query="Select * from approved WHERE approved_ID="&approved_ID
	rs2.open query, conn
	if (rs2.eof) then
		approved=UNDEFINED_VAR
	else
		approved=rs2("name")
	end if
	rs2.close
end if
%><tr><td colspan="5" style="margin: 0px; padding: 0px;"><form class="timesheet" action="edittime.asp" method="POST"><table width="100%" height="100%" style="background-color:#FFFFFF;"><tr><td valign="top" style="width:90px;"><%if isnull(rs("task_ID")) then%><%=rs("date_work")%><%else%><%=FormatDateTime(LocalizeDate(rs("date_completed")), vbShortDate)%><%end if%></td><td valign="top" style="width:80px;"><div id="time<%=rs("notes.notes_ID")%>"><%if isnull(rs("task_ID")) then%><%=FormatNumber(rs("hours"), 2)%></div><b><%=billable%></b><%if (rs("hours")>0) then%><br/><b class="approved<%=approved_ID%>"><%if (user_rights(39)) and (approved_ID=1) then%><a href="javascript:approvenote('<%=note_ID%>');"><%=approved%></a><%else%><%=approved%><%end if%></b><%end if%><%else%><b>Task</b><%end if%></td><td valign="top" style="width:100px;"><a href="notes.asp?city=<%=job_ID%>#note_<%=note_ID%>"><%=customer_name%></a></td><td valign="top" style="width:100px;"><a href="notes.asp?city=<%=job_ID%>#note_<%=note_ID%>"><%=job_name%></a></td><td valign="top">
<%if isnull(rs("task_ID")) then%><%if ((DateDiff("d", curdate, curtimedate)<=0) or (user_rights(24))) and (1=0) then %><div id="edit<%=rs("notes_id")%>" style="float:right;"><a href="javascript:edit_timesheet('<%=rs("notes_id")%>');">Edit</a>&nbsp;<a href="javascript:delete_timesheet('<%=rs("notes_id")%>');">Delete</a></div><%end if%>
<div id="info<%=rs("notes.notes_ID")%>"><%=rs("note")%></div>
<%if (editor_name<>"") then%><div id="editinfo<%=rs("notes.notes_ID")%>" style="font-size:12px;"><br>Last edited by <%=editor_name%> at <%=LocalizeDate(rs("dateedited"))%></div><%end if%>
<%else%>
<a href="notes.asp?city=<%=rs("job_ID")%>&note=<%=rs("task.notes_ID")%>#note_<%=rs("task.notes_ID")%>"><%=rs("summary")%></a> completed on <%=formatmydatetime(LocalizeDate(rs("date_completed")))%>
<%end if%></td></tr></table></form></td></tr>
<%
rs.movenext
loop
rs.close
%>
<tr><td colspan="5" align="center"><input type="button" value="Close" onclick="window.location='timesheet.asp?curdate='+escape('<%=curdate%>')+'&user=<%=timesheet_user_ID%>';" /></td></tr>
</table>
<!--#include file="bodyclose.asp" --></body></html>