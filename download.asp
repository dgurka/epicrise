<!--#include file="header.asp" -->
<% 
Const adTypeBinary = 1
if (request("file")="") then 
	%>File not Found!<%
	Response.End
end if

if not(isNumeric(request("file"))) then
	%>File not Found!<%
	Response.End
end if

query="Select * FROM inventory WHERE inventory_ID = " & request("file")
rs.open query, conn

filepath=Server.MapPath("db/inventory/" & rs("path"))
filename = Replace(rs("file_name"), " ", "_")
set fs = CreateObject("Scripting.FileSystemObject")
if (fs.FileExists(filepath)) then
	Set objStream = Server.CreateObject("ADODB.Stream")
	objStream.Open
	objStream.Type = adTypeBinary
	objStream.LoadFromFile filepath
	
	size = objStream.Size
	
	Response.AddHeader "content-disposition", "filename=" & filename
	Response.AddHeader "content-length", size
	Response.Charset = "UTF-8"

	'Response.ContentType = "application/octet-stream"
	
	Select Case LCase(fs.GetExtensionName(filepath))
		Case "jpg"
			Response.ContentType = "image/jpeg"
		Case "jpeg"
			Response.ContentType = "image/jpeg"
		Case "gif"
			Response.ContentType = "mage/gif"
		Case "png"
			Response.ContentType = "image/png"
		Case "tiff"
			Response.ContentType = "image/tiff"
		Case "tif"
			Response.ContentType = "image/tiff"
		Case "pdf"
			Response.ContentType = "application/pdf"
		Case Else
			Response.ContentType = "application/octet-stream"
	End Select
	
	i = 0
	do until i>size
		if (size-i)>100000 then
			Response.BinaryWrite(objStream.Read(100000))
		else
			Response.BinaryWrite(objStream.Read(size-i))
		end if
		Response.Flush
		i = i + 100000
	loop
	response.end
	
	objStream.Close
	set objStream = nothing
else
	%>File not Found!<%
end if
set fs = nothing
%>