<!--#include file="header.asp" --><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Timesheet</title>
<style>
table {
background-color:#000000;
width:800px;
margin-left:auto;
margin-right:auto;
}
td {
background-color:#FFFFFF;
padding: 4px;
}
</style></head>
<%
timesheet_user_ID=Request.Cookies("EPICRISE_user")
timesheet_offset=0
if (user_rights(24)) then
if (request.querystring("user")<>"") then
timesheet_user_ID=request.querystring("user")
end if
end if
if (request.querystring("offset")<>"") then
offset=request.querystring("offset")
end if
query="Select * from users where users_ID="&timesheet_user_ID
rs.open query, conn
fullname=rs("full_name")
rs.close%>
<body><!--#include file="menu.asp" -->
<!--#include file="bodyopen.asp" -->
<div style="font-size:18px;margin-right:auto;margin-left:auto;width:800px"><%=fullname%>'s Timesheet</div>
<div style="font-size:5px;"><br></div>
<table style="background-color:#FFFFFF;"><tr><td style="text-align:left;"><a href="timesheet.asp?offset=<%=(offset-1)%><%if (timesheet_user_ID<>Request.Cookies("timetracker_user")) then%>&user=<%=timesheet_user_ID%><%end if%>">Prev Timesheet</a></td><td style="text-align:center;"><a href="timesheet.asp?offset=0<%if (timesheet_user_ID<>Request.Cookies("timetracker_user")) then%>&user=<%=timesheet_user_ID%><%end if%>">Today's Timesheet</a></td><td style="text-align:right;"><a href="timesheet.asp?offset=<%=(offset+1)%><%if (timesheet_user_ID<>Request.Cookies("timetracker_user")) then%>&user=<%=timesheet_user_ID%><%end if%>">Next Timesheet</a></td></tr></table>
<table><tr style="font-weight:bold;"><td>Date</td><td>Total Hours</td><td>View Timesheet</td></tr>
<%
complete_total=0
curdate=date()
if (request.querystring("curdate")<>"") then
curdate=request.querystring("curdate")
end if
if (day(curdate)>15) then
curdate=DateAdd("d",16-day(curdate),curdate)
else
curdate=DateAdd("d",1-day(curdate),curdate)
end if
i=offset
do while (i<>0)
if (i>0) then
if (day(curdate)=16) then
curdate=DateAdd("d",-15,curdate)
curdate=DateAdd("m",1,curdate)
else
curdate=DateAdd("d",15,curdate)
end if
i=i-1
else
if (day(curdate)=16) then
curdate=DateAdd("d",-15,curdate)
else
curdate=DateAdd("m",-1,curdate)
curdate=DateAdd("d",15,curdate)
end if
i=i+1
end if
loop
timesheetdate=curdate
if (day(curdate)=1) then
enddate=DateAdd("d",15,curdate)
else
enddate=DateAdd("d",-15,curdate)
enddate=DateAdd("m",1,enddate)
end if
do while curdate<>enddate 
query="Select SUM(hours) AS total from notes where date_work=#"&curdate&"# and users_ID="&timesheet_user_ID
rs.open query,conn
total=rs("total")
if isNull(total) then 
	total=-1
else 
	complete_total=complete_total+total
end if
rs.close
total=FormatNumber(total,2)
if (total="-1.00") then total="zero" else total="<b style=""color:#2030FF"">"&total&"</b>"
%><tr><td><%=curdate%></td><td><%=total%></td><td><form method="POST" action="viewday.asp" style="display:inline;"><input type="hidden" name="user" value="<%=timesheet_user_ID%>" /><input type="hidden" name="curdate" value="<%=curdate%>" /><input type="submit" value="View"<%if total="zero" then%> disabled<%end if%> /></form></td></tr>
<%
curdate=DateAdd("d",1,curdate)
loop
%>
<tr><td><b>Grand Total</b></td>
	<td><b style="color:#2030FF"><%=FormatNumber(complete_total,2)%></b></td>
	<td>&nbsp;</td>
</table>
<div style="width:800px;text-align:center;margin-left:auto;margin-right:auto;"><%if (user_rights(24)) then%><form method="POST" action="summary.asp" style="display:inline;"><input type="hidden" name="curdate" value="<%=timesheetdate%>" /><input type="submit" value="Summary of Hours" /></form><%end if%><form method="POST" action="viewperiod.asp" style="display:inline;"><input type="hidden" name="user" value="<%=timesheet_user_ID%>" /><input type="hidden" name="curdate" value="<%=timesheetdate%>" /><input type="submit" value="View Entire Period" /></form></div>
<!--#include file="bodyclose.asp" --></body></html>