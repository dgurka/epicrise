<!--#include file="header.asp" --><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Summary of Hours</title>
<style>
table {
background-color:#000000;
width:800px;
}
td {
background-color:#FFFFFF;
padding: 4px;
}
</style></head>
<%
if not(user_rights(24)) then
response.redirect "defaultpage.asp"
end if
%>
<body><!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<div style="font-size:18px;">Summary of Hours</div>
<div style="font-size:5px;"><br></div>
<div style="width:800px;">
<%
curdate=request.form("curdate")
if (day(curdate)>15) then
curdate=DateAdd("d",16-day(curdate),curdate)
else
curdate=DateAdd("d",1-day(curdate),curdate)
end if
if (day(curdate)=1) then
enddate=DateAdd("d",15,curdate)
else
enddate=DateAdd("d",-15,curdate)
enddate=DateAdd("m",1,enddate)
end if
%>
Summary of Hours for <%=curdate%> to <%=DateAdd("d",-1,enddate)%><br/>
Run Date: <%=now()%>
<table id="htable"><tr style="font-weight:bold;"><td>Employee</td>
<%tempdate=curdate
i=0
do while (DateDiff("d",tempdate,enddate)>0)
%><td<%if (DateDiff("d", tempdate, date())<0) then%> style="color:#CC0000;"<%end if%>><%=WeekdayName(Weekday(tempdate), true)%><br/><%=Left(tempdate, Len(tempdate)-5)%></td><%
tempdate=DateAdd("d", 1, tempdate)
i=i+1
loop
daycount=i
%><td>Total</td></tr>
<% dim totals()
redim totals(i)
query="Select * from `users` WHERE active=1 order by full_name ASC"
rs.open query,conn
do until rs.eof %>
<tr><td><%=rs("full_name")%></td><%
tempdate=curdate
emptotal=0
i=0
do while (DateDiff("d", tempdate,enddate)>0)
query="Select sum(hours) as total from notes where users_ID="&rs("users_ID")&" and date_work=#"&tempdate&"#"
rs2.open query,conn
total=rs2("total")
if Not(isNull(total)) then 
emptotal=emptotal+total
totals(i)=totals(i)+total
end if
if isNull(total) then total="zero" else total="<b style=""color:#2030FF"">"&FormatNumber(total,2)&"</b>"
%><td><%=total%></td><%
rs2.close
tempdate=DateAdd("d", 1, tempdate)
i=i+1
loop
if emptotal=0 then emptotal="zero" else emptotal="<b style=""color:#2030FF"">"&FormatNumber(emptotal,2)&"</b>"
%><td><%=emptotal%></td></tr>
<%
rs.movenext
loop
rs.close
%>
<tr><td style="font-weight:bold;">Total</td><%
i=0
bigtotal=0
do while (i<daycount)
if isNull(totals(i)) or totals(i)=0 then total="zero" else total="<b style=""color:#2030FF"">"&FormatNumber(totals(i),2)&"</b>"
%><td><%=total%></td><%
if (not(isnull(totals(i)))) then bigtotal=bigtotal+totals(i)
i=i+1
loop
if bigtotal=0 then total="zero" else total="<b style=""color:#2030FF"">"&FormatNumber(bigtotal,2)&"</b>"
%><td><%=total%></td></tr>
</table>
<div style="width:800px;text-align:center;"><input type="button" value="Close" onclick="window.location='timesheet.asp?curdate='+escape('<%=curdate%>')+'&user=<%=timesheet_user_ID%>';" /></div>
</div>
<!--#include file="bodyclose.asp" --></body></html>