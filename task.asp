<!--#include file="header.asp" -->
<!--#include file="freeaspupload.asp" -->
<%if not(user_rights(36)) then server.transfer "defaultpage.asp"
task_ID=cleanInput(request("task_ID"))

query="Select * from task where task_ID="&task_ID
rs.open query, conn

task_summary = rs("summary")
task_date_due = rs("date_due")
task_date_assigned = rs("date_assigned")
notes_ID = rs("notes_ID")

rs.close

query="Select * from notes where notes_ID="&notes_ID
rs.open query, conn

note_note = rs("note")

rs.close

'test tasks

%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Move Task</title>
<style>
	table.notestable {
		background-color: black;
	}
	table.notestable tr {
		background-color: white;
	}
	table.notestable td {
		vertical-align: top;
		padding: 10px;
	}
</style>

<script type="text/javascript">

function changeStatus() {
	var loc = "taskchange.asp?task_ID=" + <%= task_ID %>
	window.location = loc
}

function moveTask() {
	var loc = "movetask.asp?task_ID=" + <%= task_ID %>
	window.location = loc
}

</script>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<h3><%= task_summary %></h3>
<table>
	<tr>
		<td>
			<table>
				<tr>
					<td>Date Assigned</td>
					<td><%= FormatDateTime(LocalizeDate(task_date_assigned), vbShortDate) %></td>
				</tr>
				<tr>
					<td>Date Due</td>
					<td><%= task_date_due %> <button onclick="moveTask()">Move</button></td>
				</tr>
				<tr>
					<td>Assigned To</td>
					<td>Joseph</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>Assigned</td>
				</tr>
			</table>
			Details:
		</td>
	</tr>
	<tr>
<td>
<table class="notestable" cellpadding="0" cellspacing="1">
	<tr>
		<td><%= note_note %></td>
	</tr>
</table>
<button onclick="changeStatus()">Update Task</button>
<h3>Notes:</h3>
<%
query="SELECT * FROM notes as n INNER JOIN users as u ON n.users_ID = u.users_ID WHERE n.notes_ID IN (14004,14005,14006)"
rs.open query, conn

do until rs.eof
	%>
	<table class="notestable" cellpadding="0" cellspacing="1">
		<tr>
			<td class="metadata">
				<div style="float:right;font-size:smaller;">#<%=  rs("notes_ID") %></div>
				<div style="font-size:larger;"><%= rs("name") %></div>
				<%= rs("dateposted") %>
			</td>
			<td><%= rs("note") %></td>
		</tr>
	</table>
	<%
	rs.movenext
loop
rs.close
%>
</td>
	</tr>
</table>
<!--#include file="bodyclose.asp" --></body></html>