<style type="text/css">
#menudiv {
	width:100%;
	text-align: center;
	border-style: none;
	margin-bottom:10px;
}

#menudiv a:link {
	color:#3333ff;
	position: relative;
	top:-1px;
	background-color: #eeeeff;
	padding: 5px;
	border: solid 1px #6666ff;
}
#menudiv a:visited {
	color:#3333ff;
	position: relative;
	top:-1px;
	background-color: #eeeeff;
	padding: 5px;
	border: solid 1px #6666ff;
}
#menudiv a:hover {
	color:#0000ff;
	position: relative;
	top:-5px;
	background-color: #ffffbb;
	padding: 5px;
	padding-bottom:9px;
	border: solid 1px #888800;
}
#menudiv a:active {
	color:#3333ff;
	position: relative;
	top:-1px;
	background-color: #eeeeff;
	padding: 5px;
	border: solid 1px #6666ff;
}

#menudiv a.exitlink:link {
	color:#ff3333;
	position: relative;
	top:-1px;
	background-color: #ffcccc;
	padding: 5px;
	border: solid 1px #ff4444;
}
#menudiv a.exitlink:visited {
	color:#ff3333;
	position: relative;
	top:-1px;
	background-color: #ffcccc;
	padding: 5px;
	border: solid 1px #ff4444;
}
#menudiv a.exitlink:hover {
	color:#ff0000;
	position: relative;
	top:-5px;
	background-color: #ffffbb;
	padding: 5px;
	padding-bottom:9px;
	border: solid 1px #888800;
}
#menudiv a.exitlink:active {
	color:#ff3333;
	position: relative;
	top:-1px;
	background-color: #ffcccc;
	padding: 5px;
	border: solid 1px #ff4444;
}

#menudiv span {
	font-size:30px;
}

b.approved1
{
	color:darkred;
}
b.approved1 a:link {
	color:darkred;
}
b.approved1 a:visited {
	color:darkred;
}
b.approved1 a:hover {
	color:red;
}
b.approved1 a:active {
	color:darkred;
}
b.approved2
{
	color:darkgreen;
}
table.workflow {
	width: 900px;
}
table.pages {
	margin-top: 10px;
	width: 900px;
}
table.navigation {
	width: 900px;
}
div.content {
	width:<%if (request("fullscreen")="1") then%>100%<%else%>1000px<%end if%>;
	margin-left:auto;
	margin-right:auto;
	padding:0px;
}
table.notestable {
	margin-top:10px;
	width: 900px;
}
table.notestable.selected td {
	background-color: #FFFFBB;
}
table.notestable td.body div.body {
	word-wrap: break-word;
	overflow:hidden;
}
.tasksexist table.notestable {
	width: 700px;
}
table.notestable td.metadata {
	width:175px;
}
td.body div.body {
	width: 700px;
}
.tasksexist td.body div.body {
	width: 500px;
}
td.tasks {
	padding-left: 10px;
}
.notestable .task {
	margin-top: 5px;
	border: solid 1px black;
	padding: 5px;
	background-color: #FFFFCC;
}

.notestable .task.overdue {
	background-color: #FFCCCC;
}

.notestable .task.completed {
	background-color: #CCFFCC;
}

.notestable .task .name {
	font-weight: bold;
}
</style>
<style type="text/css" media="print">
form,
div#menudiv,
table.notestable,
table.tasks,
table.pages,
table.navigation,
input[type="button"],
input[type="submit"],
div.editbox {
	display: none;
}

form.timesheet {
	display: block;
}

td.body div.body,
table.workflow,
.tasksexist td.body div.body,
table.notestable.selected,
div.content {
	width: 100%;
}
table.notestable.selected {
	display: table;
	background-color: transparent;
	color: black;
}
table.notestable.selected td.metadata {
	border-right: solid 1px black;
}
a {
	color: black;
}
</style>