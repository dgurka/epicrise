<!--#include file="header.asp" -->
<% 
if not((user_rights(2)) or (user_rights(3)) or (user_rights(4)) or (user_rights(5)) or (user_rights(6)) or (user_rights(7))) then 
response.Redirect("defaultpage.asp")
end if
'DATABSE FIXES
query="Select * from job WHERE register_date is NULL"
rs.open query, conn
do until rs.eof
	query="UPDATE job SET register_date=NOW() WHERE job_ID="&rs("job_ID")
	rs2.open query, conn
	rs.movenext
loop
rs.close
'END DATABASE FIXES
if (request.Cookies("tab")="") then 
	if (user_rights(2)) then 
		currenttab=1 
	elseif (user_rights(3)) then
		currenttab=2
	elseif (user_rights(4)) then
		currenttab=3
	elseif (user_rights(5)) then
		currenttab=4
	elseif (user_rights(6)) then
		currenttab=6
	elseif (user_rights(7)) then
		currentab=5
	else
		currenttab=0
	end if
	response.Cookies("tab")=currenttab
else
	currenttab=request.Cookies("tab")
end if
if (isNull(Request.Cookies("showarchive")) or Request.Cookies("showarchive")="") then
	Response.Cookies("showarchive")=0
end if
if (request.form("setarchive")="0" or request.form("setarchive")=1) then
	Response.Cookies("showarchive")=request.form("setarchive")
end if
Dim sortcols(8)
sortcols(0)="client.name+state"
sortcols(1)="job.name"
sortcols(2)="demo"
sortcols(4)="close_date"
sortcols(5)="amount"
sortcols(6)="chance"
sortcols(7)="(amount*chance)"

if (IsNull(request.querystring("sort")) or (request.querystring("sort")="")) then
	sort=4
else
	sort=request.querystring("sort")
end if
sortmode="ASC"
sortmodenum=0
if (IsNull(request.querystring("sortmode"))=false) then 
	if (request.querystring("sortmode")="1") then 
		sortmode="DESC"
		sortmodenum=1
	end if
end if

if ((request.form("action")="delete") and (IsNull(request.form("city"))=0) and (request.form("city")<>"")) then
	if (user_rights(26)) then
		city=request.form("city")
		rs.open "Select * from job WHERE job_ID="&city
		client_ID=rs("client_ID")
		rs.close
		rs.open "Delete from job where job_ID="&city
		rs.open "Delete from interest where job_ID="&city
		rs.open "Delete from notes where job_ID="&city
		rs.open "Delete from commission where job_ID="&city
		rs.open "Delete from inventory where job_ID="&city
		rs.open "Delete from call where job_ID="&city
		rs.open "Select * from job WHERE client_ID="&client_ID
		if (rs.eof) then
			rs.close
			rs.open "Delete from client WHERE client_id="&client_ID
		else
			rs.close
		end if
	end if
end if

divopen="<div id=""pipelinedata%s"" class=""pipelinedata"">"
tableopen="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:1000px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th><a href=""pipeline.asp?sort=2&amp;sortmode="&(1-sortmodenum)&""">Demo</a></th><th>Interests</th><th><a href=""pipeline.asp?sort=4&amp;sortmode="&(1-sortmodenum)&""">Est. Close</a></th><th><a href=""pipeline.asp?sort=5&amp;sortmode="&(1-sortmodenum)&""">Amount</a></th><th><a href=""pipeline.asp?sort=6&amp;sortmode="&(1-sortmodenum)&""">Chance</a></th><th><a href=""pipeline.asp?sort=7&amp;sortmode="&(1-sortmodenum)&""">Est. Return</a></th><th>Cont</th><th>Inv</th><th>Edit</th></tr>"
tableopenclosed="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:1000px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th>Closer</th><th><a href=""pipeline.asp?sort=2&amp;sortmode="&(1-sortmodenum)&""">Demo</a></th><th>Interests</th><th><a href=""pipeline.asp?sort=4&amp;sortmode="&(1-sortmodenum)&""">Close Date</a></th><th><a href=""pipeline.asp?sort=5&amp;sortmode="&(1-sortmodenum)&""">Amount</a></th><th>Cont</th><th>Inv</th><th>Edit</th></tr>"
tableopenlost="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:1000px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th>Owner</th><th><a href=""pipeline.asp?sort=2&amp;sortmode="&(1-sortmodenum)&""">Demo</a></th><th>Interests</th><th><a href=""pipeline.asp?sort=4&amp;sortmode="&(1-sortmodenum)&""">Lost Date</a></th><th><a href=""pipeline.asp?sort=5&amp;sortmode="&(1-sortmodenum)&""">Amount</a></th><th>Cont</th><th>Inv</th><th>Edit</th></tr>"
tableopenimplement="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:1200px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th><a href=""pipeline.asp?sort=4&amp;sortmode="&(1-sortmodenum)&""">Close Date</a></th><th><a href=""pipeline.asp?sort=5&amp;sortmode="&(1-sortmodenum)&""">Amount</a></th><th>HD</th><th>FAF</th><th>PF</th><th>HD</th><th>FAF</th><th>PF</th><th>Check#</th><th>Invoice#</th><th>Paid</th><th>Cont</th><th>Inv</th><th>Edit</th></tr>"

tableopencustomer="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:1000px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th><a href=""pipeline.asp?sort=2&amp;sortmode="&(1-sortmodenum)&""">Demo</a></th><th>Interests</th><th><a href=""pipeline.asp?sort=4&amp;sortmode="&(1-sortmodenum)&""">Close</a></th><th>Cont</th><th>Inv</th><th>Edit</th></tr>"

tableopensmall="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:300px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=2&amp;sortmode="&(1-sortmodenum)&""">Job</a></th></tr>"
tableopenclosedsmall="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:400px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th>Closer</th></tr>"
tableopenlostsmall="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:400px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th>Owner</th></tr>"
tableopenimplementsmall="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:400px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=1&amp;sortmode="&(1-sortmodenum)&""">Job</a></th><th>Closer</th></tr>"
tableopencustomersmall="<table align=""left"" id=""pipeline%s"" cellpadding=2 cellspacing=1 style=""border-style:none;background-color:#000000;width:300px;float:left;""><tr style=""background-color:#FFFFFF;""><th><a href=""pipeline.asp?sort=0&amp;sortmode="&(1-sortmodenum)&""">Client</a></th><th><a href=""pipeline.asp?sort=2&amp;sortmode="&(1-sortmodenum)&""">Job</a></th></tr>"

Sub WritePipeline(user, allfields)
	WritePipeline2 user, allfields, 0
End Sub

Sub WritePipeline2(user, allfields, other)
	subtotalmonth=0
	annualtotalyear=0
	set rs2 = Server.CreateObject("ADODB.Recordset")
	if (user>0) then
		sqlquery="Select job.*, client.* from job INNER JOIN client ON job.client_ID=client.client_ID where job.job_status_ID=1 and job.owner="&user& " and job.archive="&Request.Cookies("showarchive")&" ORDER BY "&sortcols(sort)&" "&sortmode
	else
		sqlquery="Select job.*, client.* from job INNER JOIN client ON job.client_ID=client.client_ID where job.job_status_ID="&(-user)& " and job.archive="&Request.Cookies("showarchive")&" ORDER BY "&sortcols(sort)&" "&sortmode
	end if
	rs.open sqlquery, conn
	if rs.eof then
		response.write "<tr style=""background-color:#FFFFFF;font-weight:bold;""><td colspan=15 align=""center"">No cities/counties Registered</td></tr>"
	else
		totalamount=0
		avgchance=0
		totalreturn=0
		count=0
		do
			if (rs.eof=false) then
				job_id=rs("job_ID")
				client_id=rs("job.client_ID")
				city=rs("client.name")
				if (rs("state")<>"") and not(isNull(rs("state"))) then city = city & ", " & UCase(rs("state"))
				state=rs("job.name")
				job_status_ID=rs("job_status_ID")
				demo=rs("demo")
				close=rs("close_date")
				amount=rs("amount")
				chance=rs("chance")
				owner=rs("owner")
				return=amount*chance/100
				contact_name=rs("contact_name")
				contact_phone=rs("contact_phone")
				contact_email=rs("contact_email")
				check=rs("check")
				invoice=rs("invoice")
				if (check="" or isNull(check)) then check="Not Paid"
				if (invoice="" or isNull(invoice)) then invoice="No Invoice"
				if ((isNull(contact_name)) and (isNull(contact_phone))) or (contact_name="" and contact_phone="") then
					contact="<td>None</td>"
				else
					contact="<td id=""contacttd"&job_id&""" onmouseover=""showcontact("&job_id&", this);"">Hover<div style=""display:none;"" id=""contact"&job_id&"""><b>Contact Name:</b>&nbsp;"&contact_name&"<br/><b>Contact Phone:</b>&nbsp;"&contact_phone&"<br/><b>Contact Email:</b>&nbsp;"&contact_email&"</div></td>"
				end if


				totalamount=totalamount + amount
				avgchance=avgchance + chance
				totalreturn=totalreturn + return

				if demo=0 then
					demo="No"
				else
					demo="Yes"
				end if

				interests=""
				rs2.open "Select * from interest where job_ID="&job_id, conn
				i=0
				do until rs2.eof
					if (((i mod 4)=0) and (i<>0)) then interests=interests & "<br/>"
					interests=interests & replace(rs2.Fields.Item("name"), " ", "&nbsp;") & ", "
					i = i + 1
					rs2.MoveNext
				loop
				rs2.close
				if interests="" then
					interests="None"
				else
					interests=Left(interests, Len(interests)-2)
				end if
			end if

			if (sort=4) then 

				'monthly subtotals
				if (rs.eof) then close=DateAdd("m", 1, close)
				if (subtotalmonth=Month(close)) and (annualtotalyear=Year(close)) then
					subtotalamount=subtotalamount+amount
					subtotalchance=subtotalchance+chance
					subtotalreturn=subtotalreturn+return
					subtotalcount=subtotalcount+1
				else
					if (subtotalmonth<>0) then
						subtotalchance=subtotalchance/subtotalcount
						if (job_status_ID=1) and (allfields<>0) then 
							response.write "<tr style=""height:40px;vertical-align:top;background-color:#FFFFFF;font-weight:bold;""><td colspan=5>"&MonthName(subtotalmonth)&" "&subtotalyear&" Totals:</td><td>"&FormatCurrency(subtotalamount)&"</td><td>"&FormatPercent(subtotalchance/100,2)&"</td><td colspan=4>"&FormatCurrency(subtotalreturn)&"</td></tr></tbody></table>"
							if ((user=Request.Cookies("EPICRISE_user")) or ((user>0) and (user_rights(1))) or (((user=-2) and (user_rights(8))) or ((user=-3) and (user_rights(9))) or ((user=-4) and (user_rights(11))) or ((user=-5) and (user_rights(10))))) then
								response.write Replace(tableopen, "%s", user & subtotalmonth & subtotalyear)
							else
								response.write Replace(tableopensmall, "%s", user & subtotalmonth & subtotalyear)
							end if
						end if
					end if
					subtotalmonth=Month(close)
					subtotalyear=Year(close)
					subtotalamount=amount
					subtotalchance=chance
					subtotalreturn=return
					subtotalcount=1
				end if

				'annual subtotals
				if (rs.eof) then 
					close=DateAdd("m", -1, close)
					close=DateAdd("yyyy", 1, close)
				end if
				if annualtotalyear=Year(close) then
					annualtotalamount=annualtotalamount+amount
					annualtotalchance=annualtotalchance+chance
					annualtotalreturn=annualtotalreturn+return
					annualtotalcount=annualtotalcount+1
				else
					if (annualtotalyear<>0) then
						annualtotalchance=annualtotalchance/annualtotalcount
						if (job_status_ID=1) and (allfields<>0) then 
							response.write "<tr style=""height:40px;vertical-align:top;background-color:#FFFFFF;font-weight:bold;""><td colspan=5>"&annualtotalyear&" Totals:</td><td>"&FormatCurrency(annualtotalamount)&"</td><td>"&FormatPercent(annualtotalchance/100,2)&"</td><td colspan=4>"&FormatCurrency(annualtotalreturn)&"</td></tr></tbody></table>"
							if ((user=Request.Cookies("EPICRISE_user")) or ((user>0) and (user_rights(1))) or (((user=-2) and (user_rights(8))) or ((user=-3) and (user_rights(9))) or ((user=-4) and (user_rights(11))) or ((user=-5) and (user_rights(10))))) then
								response.write Replace(tableopen, "%s", user & subtotalmonth & subtotalyear & "tot")
							else
								response.wrtite Replace(tableopensmall, "%s", user & subtotalmonth & subtotalyear & "tot")
							end if
						end if
					end if
					annualtotalyear=Year(close)
					annualtotalamount=amount
					annualtotalchance=chance
					annualtotalreturn=return
					annualtotalcount=1
				end if
			end if
		
		
			if (rs.eof) then Exit Do

			response.write("<tr style=""background-color:#FFFFFF;""><td>"&city&"</td>") 
			if (job_status_ID=2) then
				rs2.open "Select * from users where users_ID="&owner, conn
				closername=rs2("full_name")
				rs2.close
				if (allfields=0) and (owner<>Request.Cookies("EPICRISE_user")) then
					totalamount = totalamount - amount
					response.write "<td>"&state&"</td><td colspan=8>"&closername&"</td></tr>"
				else
					response.write "<td>"&state&"</td><td>"&closername&"</td><td>"&demo&"</td><td>"&interests&"</td><td>"&close&"</td><td>"&FormatCurrency(amount)&"</td>"&contact&"<td><a href=""inventory.asp?cust="&job_id&""">Inv</a></td><td align=""center"">"
					if (user_rights(27)) then
						response.write "<form action=""register.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""edit"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Edit"" /></form>"
					end if
					if (user_rights(28)) then
						response.write "&nbsp;<form action=""notes.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Notes"" /></form>"
					end if
					if (user_rights(26)) then
						response.write "&nbsp;<form action=""pipeline.asp"" method=""POST"" id=""delete"&job_id&""" onSubmit=""return confirmDel()"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""delete"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Delete"" /></form>"
					end if
					response.write "</td></tr>"
				end if
			elseif (job_status_ID=5) then
				if (allfields=0) and (owner<>Request.Cookies("EPICRISE_user")) then
					totalamount = totalamount - amount
					response.write "<td>"&state&"</td></tr>"
				else
					response.write "<td>"&state&"</td><td>"&demo&"</td><td>"&interests&"</td><td>"&close&"</td>"&contact&"<td><a href=""inventory.asp?cust="&job_id&""">Inv</a></td><td align=""center"">"
					if (user_rights(27)) then
						response.write "<form action=""register.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""edit"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Edit"" /></form>"
					end if
					if (user_rights(28)) then
						response.write "&nbsp;<form action=""notes.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Notes"" /></form>"
					end if
					if (user_rights(26)) then
						response.write "&nbsp;<form action=""pipeline.asp"" method=""POST"" id=""delete"&job_id&""" onSubmit=""return confirmDel()"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""delete"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Delete"" /></form>"
					end if
					response.write "</td></tr>"
				end if
			elseif (job_status_ID=4) then
				rs2.open "Select * from users where users_ID="&owner, conn
				closername=rs2("full_name")
				rs2.close
				if (allfields=0) and (owner<>Request.Cookies("EPICRISE_user")) then
					totalamount = totalamount - amount
					response.write "<td>"&state&"</td><td colspan=8>"&closername&"</td></tr>"
				else
					response.write "<td>"&state&"</td><td>"&closername&"</td><td>"&demo&"</td><td>"&interests&"</td><td>"&close&"</td><td>"&FormatCurrency(amount)&"</td>"&contact&"<td><a href=""inventory.asp?cust="&job_id&""">Inv</a></td><td align=""center"">"
					if (user_rights(27)) then
						response.write "<form action=""register.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""edit"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Edit"" /></form>"
					end if
					if (user_rights(28)) then
						response.write "&nbsp;<form action=""notes.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Notes"" /></form>"
					end if
					if (user_rights(26)) then
						response.write "&nbsp;<form action=""pipeline.asp"" method=""POST"" id=""delete"&job_id&""" onSubmit=""return confirmDel()"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""delete"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Delete"" /></form>"
					end if
					response.write "</td></tr>"
				end if
			elseif (job_status_ID=3) then
				rs2.open "Select * from users where users_ID="&owner, conn
				closername=rs2("full_name")
				rs2.close
				query="Select * from commission where job_ID="&job_id&" and commission_user_ID=1 ORDER BY commission_user_ID ASC"
				rs2.open query, conn
				if (rs2.eof) then
					fafrate=0
				else
					fafrate=rs2("rate")
					fafcheck=rs2("check")
				end if
				rs2.close
				query="Select * from commission where job_ID="&job_id&" and commission_user_ID=2 ORDER BY commission_user_ID ASC"
				rs2.open query, conn
				if (rs2.eof) then
					hdrate=0
				else
					hdrate=rs2("rate")
					hdcheck=rs2("check")
				end if
				rs2.close
				query="Select * from commission where job_ID="&job_id&" and commission_user_ID=3 ORDER BY commission_user_ID ASC"
				rs2.open query, conn
				if (rs2.eof) then
					pfrate=0
				else
					pfrate=rs2("rate")
					pfcheck=rs2("check")
				end if
				rs2.close
				if (isNull(fafcheck) and isNull(hdcheck) and isNull(pfcheck)) or (fafcheck="" and hdcheck="" and pfcheck="") then
					commchecks="<td><input type=""checkbox"" disabled /></td>"
				elseif (isNull(fafcheck) or isNull(hdcheck) or isNull(pfcheck)) or (fafcheck="" or hdcheck="" or pfcheck="") then
					commchecks="<td id=""checktd"&job_id&""" onmouseover=""showcheck("&job_id&", this);""><input type=""checkbox"" disabled /><div style=""display:none;"" id=""check"&job_id&"""><b>Hilary Check:</b>&nbsp;"& hdcheck&"<br/><b>Frank Check:</b>&nbsp;"&fafcheck&"<br/><b>Patrick Check:</b>&nbsp;"&pfcheck&"</div></td>"
				else
					commchecks="<td id=""checktd"&job_id&""" onmouseover=""showcheck("&job_id&", this);""><input type=""checkbox"" checked disabled /><div style=""display:none;"" id=""check"&job_id&"""><b>Hilary Check:</b>&nbsp;"& hdcheck&"<br/><b>Frank Check:</b>&nbsp;"&fafcheck&"<br/><b>Patrick Check:</b>&nbsp;"&pfcheck&"</div></td>"
				end if
				fafcomm=amount*fafrate/100
				hdcomm=amount*hdrate/100
				pfcomm=amount*pfrate/100
				fafratetotal=fafratetotal+fafrate
				hdratetotal=hdratetotal+hdrate
				pfratetotal=pfratetotal+pfrate
				fafcommtotal=fafcommtotal+fafcomm
				hdcommtotal=hdcommtotal+hdcomm
				pfcommtotal=pfcommtotal+pfcomm
				if (allfields=0) and (owner<>Request.Cookies("EPICRISE_user")) then
					totalamount = totalamount - amount
					response.write "<td>"&state&"</td><td colspan=14><b>Closer:</b>&nbsp;"&closername&"</td></tr>"
				else
					response.write "<td>"&state&"</td><td>"&close&"</td><td>"&FormatCurrency(amount)&"</td><td>"& FormatPercent(hdrate/100,2)&"</td><td>"&FormatPercent(fafrate/100,2)&"</td><td>"&FormatPercent(pfrate/100,2)&"</td><td>"&FormatCurrency(hdcomm)&"</td><td>"&FormatCurrency(fafcomm)&"</td><td>"&FormatCurrency(pfcomm)&"</td><td>"&check&"</td><td>"&invoice&"</td>"&commchecks&contact&"<td><a href=""inventory.asp?cust="&job_id&""">Inv</a></td><td align=""center"">"
					if (user_rights(27)) then
						response.write "<form action=""register.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""edit"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Edit"" /></form>"
					end if
					if (user_rights(28)) then
						response.write "&nbsp;<form action=""notes.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Notes"" /></form>"
					end if
					if (user_rights(26)) then
						response.write "&nbsp;<form action=""pipeline.asp"" method=""POST"" id=""delete"&job_id&""" onSubmit=""return confirmDel()"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""delete"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Delete"" /></form>"
					end if
					response.write "</tr>"
				end if
			else
				if (allfields=0) then
					response.write "<td>"&state&"</td></tr>"
				else
					response.write "<td>"&state&"</td><td>"&demo&"</td><td>"&interests&"</td><td>"&close&"</td><td>"&FormatCurrency(amount)&"</td><td>"&FormatPercent(chance/100,2)&"</td><td>"&FormatCurrency(return)&"</td>"&contact&"<td><a href=""inventory.asp?cust="&job_id&""">Inv</a></td><td align=""center"">"
					if (user_rights(27)) then
						response.write "<form action=""register.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""edit"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Edit"" /></form>"
					end if
					if (user_rights(28)) then
						response.write "&nbsp;<form action=""notes.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Notes"" /></form>"
					end if
					if (user_rights(26)) then
						response.write "&nbsp;<form action=""pipeline.asp"" method=""POST"" id=""delete"&job_id&""" onSubmit=""return confirmDel()"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""delete"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Delete"" /></form>"
					end if
					response.write "</tr>"
				end if
			end if


			rs.MoveNext
			count=count+1
		loop
		avgchance=avgchance/count
		fafratetotal=fafratetotal/count
		hdratetotal=hdratetotal/count
		pfratetotal=pfratetotal/count
		if (job_status_ID=2) then
			response.write "<tr style=""background-color:#FFFFFF;font-weight:bold;""><td colspan=6>Totals:</td><td colspan=4>"&FormatCurrency(totalamount)&"</td></tr>"
		elseif (job_status_ID=4) then
			response.write "<tr style=""background-color:#FFFFFF;font-weight:bold;""><td colspan=6>Totals:</td><td colspan=4>"&FormatCurrency(totalamount)&"</td></tr>"
		elseif (job_status_ID=5) then
			
		elseif (job_status_ID=3) then
			if (allfields<>0) then
				response.write "<tr style=""background-color:#FFFFFF;font-weight:bold;""><td colspan=2>Totals:</td><td>"&FormatCurrency(totalamount)&"</td><td>"&FormatPercent(hdratetotal/100,2)&"</td><td>"&FormatPercent(fafratetotal/100,2)&"</td><td>"&FormatPercent(pfratetotal/100,2)&"</td><td>"&FormatCurrency(hdcommtotal)&"</td><td>"&FormatCurrency(fafcommtotal)&"</td><td colspan=8>"&FormatCurrency(pfcommtotal)&"</td></tr>"
			end if
		else
			if (allfields<>0) then 
				response.write "<tr style=""background-color:#FFFFFF;font-weight:bold;""><td colspan=5>Totals:</td><td>"&FormatCurrency(totalamount)&"</td><td>"&FormatPercent(avgchance/100,2)&"</td><td colspan=5>"&FormatCurrency(totalreturn)&"</td></tr>"
			end if
		end if
	end if
	response.Write "</table>" & vbCrLf
	if (other=0) then response.write "</div>"
	rs.close
End Sub
%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title><%=name%>'s Pipeline</title>
<style type="text/css">
.tab {
	border-left: solid 1px black;
	border-right: solid 1px black;
	border-top: solid 1px black;
	border-bottom-style: none;
	text-align: center;
	cursor: pointer;<%if (Request.Cookies("showarchive")) then%>
	background-color:#ffcccc;<%end if%>
}
.tabselected {
	border-left: solid 1px black;
	border-right: solid 1px black;
	border-top: solid 1px black;
	border-bottom-style: none;
	text-align: center;
	cursor: pointer;
	background-color: #ffffbb;
}
.pipelinedata {
	display:none;
}
table
{
	margin-bottom:10px;
}
</style>
<script type="text/javascript">
function confirmDel() {
	var confirmed = confirm("Are you sure you want to delete this city?\nOnce deleted a city/county cannot be recovered.");
	if (confirmed) {
		return true;
	}
	return false;
}
function showcontact(id, obj) {
	document.getElementById('contactdisplay').innerHTML=document.getElementById('contact'+id).innerHTML;
	document.getElementById('contactdisplay').style.display="block";
	var offsetleft;
	var offsettop;
	offsetleft=obj.offsetLeft;
	offsettop=obj.offsetTop;
	while(obj = obj.offsetParent) {
		offsetleft += obj.offsetLeft;
		offsettop += obj.offsetTop;
	}
	offsetleft -= 5;
	offsetleft -= 5;
	document.getElementById('contactdisplay').style.left=offsetleft;
	document.getElementById('contactdisplay').style.top=offsettop;
}
function hidecontact() {
	document.getElementById('contactdisplay').style.display="none";
}
function showcheck(id, obj) {
	document.getElementById('checkdisplay').innerHTML=document.getElementById('check'+id).innerHTML;
	document.getElementById('checkdisplay').style.display="block";
	var offsetleft;
	var offsettop;
	offsetleft=obj.offsetLeft;
	offsettop=obj.offsetTop;
	while(obj = obj.offsetParent) {
		offsetleft += obj.offsetLeft;
		offsettop += obj.offsetTop;
	}
	offsetleft -= 5;
	offsetleft -= 5;
	document.getElementById('checkdisplay').style.left=offsetleft;
	document.getElementById('checkdisplay').style.top=offsettop;
}
function hidecheck() {
	document.getElementById('checkdisplay').style.display="none";
}
function settabcookie(tab)
{
	
	document.cookie = " tab=" + tab;
}
function showpipeline(tab, id)
{
	if (id=='others') {
		settabcookie(2);
	} else if (id==-2) {
		settabcookie(3);
	} else if (id==-3) {
		settabcookie(4);
	} else if (id==-4) {
		settabcookie(5);
	} else if (id==-5) {
		settabcookie(6);
	} else {
		settabcookie(1);
	}
	hidecheck();
	hidecontact();
	tabstable = document.getElementById("tabs");
	var elems = tabstable.getElementsByTagName("td");
	var elemsLen = elems.length;
	for (i=0; i< elemsLen; i++)
		if (elems[i].className=="tabselected")
			elems[i].className="tab"
	tab.className="tabselected";
	document.getElementById("pipelinedata"+id).style.display = "block";
	document.getElementById("pipelineshow").innerHTML=document.getElementById("pipelinedata"+id).innerHTML;
	document.getElementById("pipelinedata"+id).style.display = "none";
	if (id==-3) {
		document.getElementById("pipelineshow").style.width = '1200px';
		if (document.body.clientWidth>=1200)
			document.getElementById("pipelineshow").style.left = '-100px';
	} else {
		document.getElementById("pipelineshow").style.width = '1000px';
		document.getElementById("pipelineshow").style.left = '0px';
	}
}
function showtab(tab)
{
	if (tab==2) {
		showpipeline(document.getElementById('otherspipelinetab'), 'others');
	} else if (tab==3) {
		showpipeline(document.getElementById('closedpipelinetab'), -2);
	} else if (tab==4) {
		showpipeline(document.getElementById('implementedpipelinetab'), -3);
	} else if (tab==5) {
		showpipeline(document.getElementById('lostpipelinetab'), -4);
	} else if (tab==6) {
		showpipeline(document.getElementById('customerpipelinetab'), -5);
	} else {
		showpipeline(document.getElementById('mypipelinetab'), <%=user%>);
	}
}
</script>
</head>
<body onload="showtab(<%=currenttab%>);">
<!--#include file="menu.asp" -->
<!--#include file="bodyopen.asp" -->
<table style="width:1000px;margin-left:auto;margin-right:auto;margin-bottom:0px;" id="tabs"><tr>
<% if (user_rights(2)) then %><td id="mypipelinetab" class="tabselected" onclick="showpipeline(this, <%=user%>);">My Pipeline
<%=replace(divopen, "%s", user)%>
<%=replace(tableopen, "%s", user)%>
<%WritePipeline user, 1%></td><% end if%>
<% if (user_rights(3)) then 
	%><td id="otherspipelinetab" class="tab" onclick="showpipeline(this, 'others');">Other's Pipeline<% 
	response.write replace(divopen ,"%s", "others")
	set userrs = Server.CreateObject("ADODB.Recordset")
	userrs.open "Select * from users WHERE users_ID<>"&user, conn
	do until userrs.eof
		templvl=userrs("user_level")
		query="Select * from user_level_rights WHERE user_level_ID="&templvl&" and user_rights_ID=2"
		rs.open query, conn
		match=true
		if (rs.eof) then
			match=false
		end if
		rs.close
		if (match=true) then
			%><div style="width:1000px;font-size:16pt;font-weight:bold;"><%=userrs("full_name")%>'s Pipeline</div><%
			if (user_rights(1)) then
				response.Write replace(tableopen, "%s", userrs("users_ID"))
				WritePipeline2 userrs("users_ID"), 1, 1
			else
				response.Write replace(tableopensmall, "%s", userrs("users_ID"))
				WritePipeline2 userrs("users_ID"), 0, 1
			end if
		end if
		userrs.MoveNext
	loop
	response.write "</div></td>"
	userrs.close
end if
if (user_rights(4)) then
rs.open "Select * from job_status WHERE job_status_ID=2"
%><td id="closedpipelinetab" class="tab" onclick="showpipeline(this, -2);"><%=rs("name")%><%
rs.close
response.write replace(divopen ,"%s", -2)
if (user_rights(8)) then
response.Write replace(tableopenclosed, "%s", -2)
WritePipeline -2,1
else
response.Write replace(tableopenclosed, "%s", -2)
WritePipeline -2,0
end if
%></td><%
end if
if (user_rights(5)) then
rs.open "Select * from job_status WHERE job_status_ID=3"
%><td id="implementedpipelinetab" class="tab" onclick="showpipeline(this, -3);"><%=rs("name")%><%
rs.close
response.write replace(divopen ,"%s", -3)
if (user_rights(9)) then
response.Write replace(tableopenimplement, "%s", -3)
WritePipeline -3,1
else
response.Write replace(tableopenimplement, "%s", -3)
WritePipeline -3,0
end if
%></td><%
end if
if (user_rights(6)) then
rs.open "Select * from job_status WHERE job_status_ID=5"
%><td id="customerpipelinetab" class="tab" onclick="showpipeline(this, -5);"><%=rs("name")%><%
rs.close
response.write replace(divopen ,"%s", -5)
if (user_rights(10)) then
response.Write replace(tableopencustomer, "%s", -5)
WritePipeline -5,1
else
response.Write replace(tableopencustomersmall, "%s", -5)
WritePipeline -5,0
end if
%></td><%
end if
if (user_rights(7)) then
rs.open "Select * from job_status WHERE job_status_ID=4"
%><td id="lostpipelinetab" class="tab" onclick="showpipeline(this, -4);"><%=rs("name")%><%
rs.close
response.write replace(divopen ,"%s", -4)
if (user_rights(11)) then
response.Write replace(tableopenlost, "%s", -4)
WritePipeline -4,1
else
response.Write replace(tableopenlost, "%s", -4)
WritePipeline -4,0
end if
%></td>
<%end if%>
</tr></table>
<div id="pipelineshow" style="width:1000px;margin-left:auto; margin-right: auto;position:relative;left:0px;"></div>
<div style="display:none;position:absolute;left:0px;top:0px;padding:5px;background-color:#FFFFFF;border-style:solid;border-color:#000000;border-width:2px;" id="contactdisplay" onmouseout="hidecontact();"></div>
<div style="display:none;position:absolute;left:0px;top:0px;padding:5px;background-color:#FFFFFF;border-style:solid;border-color:#000000;border-width:2px;" id="checkdisplay" onmouseout="hidecheck();"></div>
<!--#include file="bodyclose.asp" --></body></html>