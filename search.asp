<!--#include file="header.asp" -->
<!--#include file="freeaspupload.asp" -->
<%

if not(user_rights(28)) then response.redirect "defaultpage.asp"

errormsg=""
warning=""

if (len(request("search"))<3) then
	errormsg="Search Must be at least 3 characters long"
end if

'Note lookup functionality
if (isNumeric(request("search"))) then
	query = "SELECT COUNT(notes_ID) AS total FROM Notes WHERE notes_ID = " & request("search")
	rs.open query, conn
	if (rs("total")>0) then
		response.redirect("notes.asp?note=" & request("search"))
	end if
	rs.close
end if

job_ID=request("job_ID")

if (job_ID="") then job_ID=0
search=Replace(request("search"),"'", "''")
search=Split(search, " ")
n = 0
for i = 0 to UBOUND(search)
	if (Len(search(i))>=3) then
		n = n + 1
	end if
next
if (n=0) then
	errormsg="Search does not have enough valid terms!"
end if
page=1
if ((IsNumeric(request("page"))) and request("page")<>"") then
	page=CInt(request("page"))
end if

scope=request("scope")
if (scope<>"1" and scope<>"2") then scope="1"

public function getwordcondition(field, value)
	getwordcondition = "(" & field & " LIKE '%[^a-z,A-Z]" & value & "[^a-z,A-Z]%' or " & field & " LIKE '" & value & "[^a-z,A-Z]%' or " & field & " LIKE '%[^a-z,A-Z]" & value & "' or " & field & " LIKE '%" & value & "%')"
end function
%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Search Results</title>
<style>
table.notestable {
	background-color: black;
}
table.notestable tr {
	background-color: white;
}
table.notestable td {
	vertical-align: top;
	padding: 10px;
}
.editbox
{
	float:right;
}
b.highlight
{
	background-color:yellow;
}
</style>
<script type="text/javascript" src="xmlHttp.js"></script>
</head>
<body>
<!--#include file="menu.asp" -->
<!--#include file="bodyopen.asp" -->
<form action="search.asp" method="POST">
<input type="hidden" name="job_id" value="<%=request("job_ID")%>" />
<table width="100%"><tr><td>
<input type="text" name="search" size="40" value="<%=request("search")%>" />
<input type="submit" value="Search" /></td></tr>
<tr><td><table><tr><td><input type="radio" name="scope" id="scope1" value="1" <%if (scope="1") then response.write"checked "%>/><label for="scope1">Matches ALL Keywords</label></td><td><input type="radio" name="scope" id="scope2" value="2" <%if (scope="2") then response.write"checked "%>/><label for="scope2">Matches ANY Keywords</label></td></tr></table></td></tr></table>
</form>
<%
if (errormsg<>"") then%>
<b style="color:red;"><%=errormsg%></b>
<%
else
if (warning<>"") then
%><b style="color:darkred;"><%=warning%></b>
<%
end if
conditions1="(" & getwordcondition("notes.note", search(0))
conditions2="(" & getwordcondition("task.summary", search(0))
if (scope="2") then
	operator="or"
else
	operator="and"
end if
for i = 1 to UBOUND(search)
	if (Len(search(i))>=3) then
		conditions1 = conditions1 & " " & operator & getwordcondition("notes.note",search(i))
		conditions2 = conditions2 & " " & operator & getwordcondition("task.summary",search(i))
	end if
next
conditions1 = conditions1 & ")"
conditions2 = conditions2 & ")"
if (job_ID<>0) then
	if (job_ID>0) then
		conditions1 = conditions1 & " and notes.job_ID=" & job_ID
		conditions2 = conditions2 & " and notes.job_ID=" & job_ID
	else
		conditions1 = conditions1 & " and job.client_ID=" & (-job_ID)
		conditions2 = conditions2 & " and job.client_ID=" & (-job_ID)
	end if
end if

if (job_ID<>"") and (job_ID>0) then
	query = "Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes left outer join task on notes.notes_ID=-task.task_ID WHERE " & conditions1 & _ 
	vbcrlf & " UNION ALL " & vbcrlf & _
	"Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes right outer join task on notes.notes_ID=task.notes_ID WHERE " & conditions2 & " and task.date_completed is not null ORDER BY datesort DESC"
else
	query = "Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from (notes left outer join task on notes.notes_ID=-task.task_ID) left join job on notes.job_ID=job.job_ID WHERE " & conditions1 & _ 
	vbcrlf & " UNION ALL " & vbcrlf & _
	"Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from (notes right outer join task on notes.notes_ID=task.notes_ID) left join job on notes.job_ID=job.job_ID WHERE " & conditions2 & " and task.date_completed is not null ORDER BY datesort DESC"
end if
countquery = "SELECT count(notes.notes_ID) as TOTAL FROM ( " & query & ")"
rs.open countquery, conn
totalitems = rs("TOTAL")
rs.close
totalpages = -Int(-totalitems/ITEMS_PER_PAGE)
if (totalitems>ITEMS_PER_PAGE) then
	query = "SELECT TOP " & page*ITEMS_PER_PAGE & " * FROM ( " & query & ") ORDER BY datesort DESC"
	if (page<>1) then
	query = "SELECT * FROM ( SELECT TOP " & ITEMS_PER_PAGE & " *  FROM ( " & query & " ) ORDER BY datesort ASC) ORDER BY datesort DESC"
	end if
end if
%><table style="margin-top:10px;width:900px;"><tr><td align="center">
Page:<% 
if (page<>1) then
	%>&nbsp;<a href="javascript:page(1)">Newest</a>&nbsp;<a href="javascript:page(<%=i-1%>)">Newer</a><%
end if
for i = 1 to totalpages
	if (i=page) then
		%>&nbsp;<b><%=i%></b><%
	else
		%>&nbsp;<a href="javascript:page(<%=i%>)"><%=i%></a><%
	end if
next
if (page<>totalpages) then
	%>&nbsp;<a href="javascript:page(<%=page+1%>)">Older</a>&nbsp;<a href="javascript:page(<%=totalpages%>)">Oldest</a><%
end if
%></td></tr></table><%
page_query=query
rs.open query, conn
if (rs.eof) then
%><table class="notestable" cellpadding=0 cellspacing=1 style="margin-top:10px;width:900px;"><tr><td style="text-align: center;font-weight:bold;">No results found</td></tr></table><%
end if
set re = new RegExp
re.pattern = "(((http://)|(https://))(([a-zA-Z0-9]+\.[a-zA-Z0-9]+\.[a-zA-Z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))[a-zA-Z0-9\-_/\\]*\.?[a-zA-Z0-9\-_#=\?\&_/\\]+)"
re.global = True
set re2 = new RegExp
re2.pattern = "^(?!http://)(?!https://)((([a-zA-Z0-9]+\.[a-zA-Z0-9]+\.[a-zA-Z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))[a-zA-Z0-9/\\]*\.?[a-zA-Z0-9#=\?\&_/\\]+)"
re2.global = True
set re3 = new RegExp
re3.IgnoreCase = True
re3.global = True
set re4 = new RegExp
re4.pattern="(\<[^ ]+ *[^ ]+=\""[^\<\""]*)\<[^\>]*\>[^\<]*\<[^\>]*\>"
re4.global = True
do until rs.eof
	if (isnull(rs("task_ID"))) then
		notes_id=rs("notes.notes_ID")
		users_ID=rs("notes.users_ID")
		dateposted=LocalizeDate(CDate(rs("dateposted")))
		note=rs("note")
		editnotetext = note
		note = re.replace(note, "<a href=""$1"">$1</a>")
		note = re2.replace(note, "<a href=""http://$1"">$1</a>")
		for i = 0 to UBOUND(search)
			if (Len(search(i))>=3) then
				re3.pattern = "((([^a-zA-Z])(" & search(i) & ")([^a-zA-Z]))|(([^a-zA-Z])(" & search(i) & "))|((" & search(i) & ")([^a-zA-Z])))"
				note = re3.replace(note, "$3$7<b class=""highlight"">$4$8$10</b>$5$11")
			end if
		next
		note = re4.replace(note, "$1")
		if (isNull(rs("hours"))) then hours=0 else hours=rs("hours")
		if (isNull(rs("date_work"))) then date_work="" else date_work=CDate(rs("date_work"))
		if (isNull(rs("billable_ID"))) then billable_ID="" else billable_ID=rs("billable_ID")
		if (isNull(rs("approved_ID"))) then approved_ID="" else approved_ID=rs("approved_ID")
		dateedited=LocalizeDate(rs("dateedited"))
		editor_ID=rs("notes.editor_ID")
		notes_ID=rs("notes.notes_ID")
		query="Select full_name, user_level from `users` WHERE users_ID="&users_ID
		rs2.open query, conn
		if (rs2.eof) then
			username=STR_USER_NOT_FOUND
			user_level_ID=-1
		else
			username=rs2("full_name")
			user_level_ID=rs2("user_level")
		end if
		rs2.close
		if ((job_ID<=0) or (job_ID="")) then 
			job_name = rs("name")
			query="Select client.name, client.state from client inner join job ON job.client_ID=client.client_ID WHERE job_ID=" & rs("notes.job_ID")
			rs2.open query, conn
			if (rs2.eof) then
				city=""
				state=""
			else
				city=rs2("name")
				state=rs2("state")
			end if
			rs2.close()
		end if
		query="Select name from billable WHERE billable_ID="&billable_ID
		rs2.open query, conn
		if (rs2.eof) then
			billable=UNDEFINED_VAR
		else
			billable=rs2("name")
		end if
		rs2.close
		if (approved_ID<>"") then
			query="Select name from approved WHERE approved_ID="&approved_ID
			rs2.open query, conn
			if (rs2.eof) then
				approved=UNDEFINED_VAR
			else
				approved=rs2("name")
			end if
			rs2.close
		end if
		if (editor_id<>"") then
			query="Select full_name from `users` WHERE users_ID="&editor_ID
			rs2.open query, conn
			if (rs2.eof) then
				editorname=STR_USER_NOT_FOUND
			else
				editorname=rs2("full_name")
			end if
			rs2.close
		end if
		query="Select name from user_level WHERE user_level_ID="&user_level_ID
		rs2.open query, conn
		if (rs2.eof) then
			user_level=""
		else
			user_level=rs2("name")
		end if
		rs2.close
%><a name="note_<%=notes_ID%>"><table class="notestable" id="notebox_<%=notes_ID%>" cellpadding="0" cellspacing="1" style="width:900px;"><tr><td style="width:175px;">
	<div style="font-size:larger;"><%=username%></div>
	<div style="font-size:smaller;"><%=user_level%><br/>
	<%=dateposted%><%if (hours<>0) then
	%><br/><br/>Hours: <%=FormatNumber(hours,2)%><br/>
	<%=billable%> - <b class="approved<%=approved_ID%>"><%=approved%></b><%if (Year(date_work)<>Year(dateposted)) or (Month(date_work)<>Month(dateposted)) or (Day(date_work)<>Day(dateposted)) then
	%><br/>Work Date: <%=date_work%><%end if%><%end if%><% if (editor_ID<>"") then
	%><br/><br/>Last edited by <%=editorname%> at<br/>
	<%=dateedited%>
	<% end if %></div>
	</td><td id="notesbox<%=notes_ID%>"><%if (job_ID<=0) then%><a href="notes.asp?note=<%=notes_ID%>"><%=city%><%if (state<>"") then%>, <%=state%><%end if%><%=" "%><%=job_name%></a>:<hr width="100%"/><%end if%><div id="noteseditbox<%=notes_ID%>" class="editbox"><a href="notes.asp?note=<%=notes_ID%>">Go to Note</a></div>
	<%=note%><%
	query = "Select * from inventory WHERE notes_ID="&notes_ID
	rs2.open query, conn
	inventory = false
	if not(rs2.eof) then 
		response.write("<hr/><table style=""width:100%;text-align:center;""><tr>")
		inventory=true
	end if
	count=0
	do until rs2.eof
	count=count+1
	%><td><a href="download.asp?file=<%=rs2("inventory_ID")%>" target="_BLANK"><%if ((LCase(Right(rs2("path"),3))="jpg") or (LCase(Right(rs2("path"),3))="gif") or (LCase(Right(rs2("path"),3))="png") or (LCase(Right(rs2("path"),4))="jpeg")) then%><img src="db/inventory/<%=rs2("path")%>" border="0" style="<%if (LCase(Right(rs2("path"),3))="png") then%>height<%else%><%if (ImgDimension(Server.MapPath("db/inventory/")&rs2("path"))) then %>width<%else%>height<%end if%><%end if%>:100px;max-width:100px;max-height:100px;" /><br/><%if (rs2("name")="") then response.write rs2("file_name") else response.write rs2("name")%><%else%><image src="file.gif" border="0"/> <%if (rs2("name")="") then response.write rs2("file_name") else response.write rs2("name")%><%end if%></a></td><%if (count mod 4)=0 then%></tr><tr><%end if
	rs2.movenext
	loop
	rs2.close
	if (inventory) then response.write("</tr></table>")
	%>
	</td></tr></table></a><br/>
	<%else
	query="Select * from users WHERE users_ID="&rs("task.users_ID")
	rs2.open query, conn
	if rs2.eof then
		user_name=STR_USER_NOT_FOUND
	else
		user_name=rs2("full_name")
	end if%>
	<table class="notestable" cellpadding="0" cellspacing="1" style="width:900px;"><tr><td><a href="javascript:shownote(<%=rs("task.notes_ID")%>)"><%=rs("summary")%></a> completed by <%=user_name%> on <%=formatmydatetime(LocalizeDate(rs("date_completed")))%></td></tr></table><br/>
	<%rs2.close
	end if
	rs.movenext
loop
rs.close
end if%>
<!--#include file="bodyclose.asp" --></body></html>