<!--#include file="localization.asp" -->
<!--#include file="hex_sha1_js.asp" --><%
set conn=Server.CreateObject("ADODB.Connection")
Conn.Mode = 3      '3 = adModeReadWrite
conn.Provider="Microsoft.Jet.OLEDB.4.0"
conn.Open Server.MapPath("db/") & "\epicrise.mdb"
set rs =Server.CreateObject("ADODB.Recordset")
set rs1=Server.CreateObject("ADODB.Recordset")
set rs2=Server.CreateObject("ADODB.Recordset")
set rs3=Server.CreateObject("ADODB.Recordset")

Response.Expires = -1

ITEMS_PER_PAGE=30

LOCAL_TIMEZONE_OFFSET=-300 ' MINUTES, i.e. UTC-5

EPI_SALT = "jk2398J(*PW#FLI#o;JF09923fe::O#JWFo;;jJK:L#ijw""fpoJiaisdpojf;woaeij3fpoj4f"

if (not(endsWith(Request.ServerVariables("SCRIPT_NAME"),"default.asp")) and not(endsWith(Request.ServerVariables("SCRIPT_NAME"),"login.asp"))) then
	if (Isnull(Request.Cookies("EPICRISE_user")) or Request.Cookies("EPICRISE_user")="") then
		response.Redirect("default.asp")
	else
		user=Request.Cookies("EPICRISE_user")
		rs.open "Select * from users where users_ID="&user, conn
		if (rs.eof) then 
			response.redirect("login.asp")
		end if
		name = rs("full_name")
		level = rs("user_level")
		rs.close
		
		query = "SELECT `Session_ID` FROM `session` WHERE `Session_ID` = '" & Request.Cookies("EPICRISE_user_verify") & "' and `users_ID` = " & user & " and `REMOTE_ADDR` = '" & Request.ServerVariables("REMOTE_ADDR") & "' and `end_date` IS NULL"
		rs.open query, conn
		if (rs.eof) then
			response.redirect("login.asp")
		end if
		rs.close
		
		query="Select Top 1 user_rights_ID from user_rights ORDER BY user_rights_ID DESC"
		rs.open query, conn
		NUM_OF_RIGHTS=rs("user_rights_ID")
		rs.close
		Dim user_rights()
		Redim user_rights(NUM_OF_RIGHTS)
		for i = 1 to NUM_OF_RIGHTS
			user_rights(i)=false
		next
		query="Select * from user_level_rights WHERE user_level_ID="&level&" ORDER BY user_rights_ID ASC"
		rs.open query, conn
		do until rs.eof
			user_rights(rs("user_rights_ID"))=true
			rs.movenext
		loop
		rs.close
		
		query="Select Top 1 notification_ID from notification ORDER BY notification_ID DESC"
		rs.open query, conn
		NUM_OF_NOTIFICATION=rs("notification_ID")
		rs.close
		Dim user_notification()
		Redim user_notification(NUM_OF_NOTIFICATION)
		for i = 1 to NUM_OF_NOTIFICATION
			user_notification(i)=false
		next
		query = "Select notification_ID from user_notification WHERE users_ID=" & user & " ORDER BY notification_ID ASC"
		rs.open query, conn
		do until rs.eof
			user_notification(rs("notification_ID"))=true
			rs.movenext
		loop
		rs.close
	end if 
end if

public function endsWith(byVal str, chars)
	endsWith = (right(str, len(chars)) = chars)
end function

function cleanInput(input)
	if (input="") or (isNull(input)) then cleanInput="" else cleanInput=Replace(Replace(Replace(Replace(Replace(Replace(input, "'", "''"), "<", "&lt;"), ">", "&gt;"), vbCrLf, "<br/>"), Chr(13), "<br/>"), Chr(10), "<br/>")
end function
function uncleanInput(input)
	if (input="") or (isNull(input)) then uncleanInput="" else uncleanInput=Replace(Replace(Replace(input, "&lt;", "<"), "&gt;", ">"), "<br/>", vbCrLf)
end function

function LocalizeDate(input)
	If (Not(IsNull(input)) AND IsDate(input)) Then
		set oShell = CreateObject("WScript.Shell") 
		atb = "HKEY_LOCAL_MACHINE\System\CurrentControlSet\" &_ 
			"Control\TimeZoneInformation\ActiveTimeBias" 
		offsetMin = oShell.RegRead(atb) 
        'Hacky DST. I'm terribly sorry whoever reads this but I don't have time for a proper implementation =(
        DSTOffset =0 'MINUTES
        if (Month(Now())>=3 and Month(Now())<=11) then
            DSTOffset=60 ' MINUTES
        end if
		LocalizeDate = dateadd("n", offsetMin + LOCAL_TIMEZONE_OFFSET + DSTOffset, input) 
	Else
		LocalizeDate=input
	End If
end function

function formatmydatetime(input)
	formatmydatetime=Replace(Replace(input, " AM", "&nbsp;AM"), " PM", "&nbsp;PM")
end function
function IIF(condition, truepart, falsepart)
	if (condition) then
		IIF=truepart
	else
		IIF=falsepart
	end if
end function

function ImgDimension(img) 
	dim iwidth, iheight 
	dim myImg, fs 
	Set fs= CreateObject("Scripting.FileSystemObject") 
	img=Server.MapPath("/file.gif")
	if not fs.fileExists(img) then 
		ImgDimension=False
		Exit function
	end if
	set myImg = loadpicture(img) 
	iwidth = round(myImg.width / 26.4583) 
	iheight = round(myImg.height / 26.4583) 
	set myImg = nothing 
	if (iwidth>iheight) then ImgDimension=True else ImgDimension=False
end function
%>