<!--#include file="header.asp" --><%
if not(user_rights(31)) then response.redirect "defaultpage.asp"

dim sortmodes(4)
sortmodes(1)="client.name & client.state"
sortmodes(2)="job.name"
sortmodes(3)="interests"
sortmodes(4)="max(date_updated_1)"
if (request("sort")<>"") then
	sortmode=CInt(request("sort"))
else
	sortmode=4
end if
if (request("dir")<>"") then
	sortdir=CInt(request("dir"))
else
	sortdir=1
end if

if (request("action")="date_due") then
	task_ID=request("task_ID")
	date_due=cleanInput(request("date_due"))
	query="UPDATE task SET date_due=#"&date_due&"# WHERE task_ID="&task_ID
	rs.open query, conn
end if
%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title><%=name%>'s Dashboard</title>
<style>
table.clienttable {
border-style:none;
background-color:#000000;
width:<%if (request("fullscreen")="1") then%>100%<%else%>700px<%end if%>;
}
table.clienttable td, th {
background-color:#FFFFFF;
}
label
{
	margin-left:10px;
}
</style>
<script type="text/javascript" src="xmlHttp.js"></script>
<script type="text/javascript">
function show_prospects(checkbox)
{
	if (checkbox.checked) {
		document.dashboard_settings_form.show_prospects.value='1';
	} else {
		document.dashboard_settings_form.show_prospects.value='0';
	}
	document.dashboard_settings_form.submit();
}
function show_clients(checkbox)
{
	if (checkbox.checked) {
		document.dashboard_settings_form.show_clients.value='1';
	} else {
		document.dashboard_settings_form.show_clients.value='0';
	}
	document.dashboard_settings_form.submit();
}
function show_howtos(checkbox)
{
	if (checkbox.checked) {
		document.dashboard_settings_form.show_howtos.value='1';
	} else {
		document.dashboard_settings_form.show_howtos.value='0';
	}
	document.dashboard_settings_form.submit();
}

var xmlHttp;
var calendarEditBox;
var calendarDisplayBox;

function calendarShow(thebox, note_ID)
{
if (calendarDisplayBox) calendarClose();
calendarEditBox = thebox;
calendarDisplayBox = document.getElementById("calendar" + note_ID);
calendarDisplayBox.style.display="block";
calendarEditBox.blur();
calendarDisplayBox.focus();
}

function calendarClose()
{
calendarDisplayBox.style.display="none";
}

function calendarSelect(month, day, year)
{
calendarDisplayBox.style.display="none";
calendarEditBox.value=month+"/"+day+"/"+year;
calendarMove(calendarEditBox.value);
}

function calendarUpdate()
{
if (xmlHttp.readyState==4)
{
calendarDisplayBox.innerHTML=xmlHttp.responseText;
}
}

function calendarMove(newdate)
{
xmlHttp=createXmlHttp();
xmlHttp.onreadystatechange=calendarUpdate;
xmlHttp.open("GET","calendar.asp?curdate="+escape(newdate),true);
xmlHttp.send(null);
}

function edit_task_date(task_ID)
{
	document.getElementById('taskdate'+task_ID).innerHTML=document.getElementById('taskdateedit'+task_ID).innerHTML;
}

function sort(sort, dir)
{
	document.dashboard_settings_form.sort.value=sort;
	document.dashboard_settings_form.dir.value=dir;
	document.dashboard_settings_form.submit();
}

function printtasks()
{
	document.dashboard_settings_form.target="_BLANK";
	document.dashboard_settings_form.menu_visible.value = "0";
	document.dashboard_settings_form.jobs_visible.value = "0";
	document.dashboard_settings_form.settings_visible.value = "0";
	document.dashboard_settings_form.fullscreen.value = "1";
	document.dashboard_settings_form.submit();
	document.dashboard_settings_form.menu_visible.value = "1";
	document.dashboard_settings_form.jobs_visible.value = "1";
	document.dashboard_settings_form.settings_visible.value = "1";
	document.dashboard_settings_form.fullscreen.value = "0";
	document.dashboard_settings_form.target="";
}
<%if (user_rights(36)) then%>
var tasktimer;
var taskhidetimer;

function mouseovertask(task)
{
	tasktimer = setTimeout('showedittask('+task+');',500);
}

function mouseouttask(task)
{
	clearTimeout(tasktimer);
	taskhidetimer = setTimeout('hideedittask('+task+');',5000);
}

function showedittask(task)
{
	document.getElementById('edit_task_'+task).style.display='block';
}

function hideedittask(task)
{
	document.getElementById('edit_task_'+task).style.display='none';
}<%end if%>
</script>
</head>
<body><%if (not(request("menu_visible")="0")) then%>
<!--#include file="menu.asp" --><%end if%>
<!--#include file="bodyopen.asp" -->
<table style="width:<%if (request("fullscreen")="1") then%>100%<%else%>900px<%end if%>;"><tr><td valign="top">
<%if not(request("jobs_visible")="0") then%><table width="100%"><tr><td>
<input type="checkbox" onclick="show_clients(this);" id="show_clients" <%if not(request("show_clients")="0") then%>checked <%end if%>/><label for="show_clients">Clients</label><br/>
<input type="checkbox" onclick="show_prospects(this);" id="show_prospects" <%if (request("show_prospects")="1") then%>checked <%end if%>/><label for="show_prospects">Prospects</label><br/>
<input type="checkbox" onclick="show_howtos(this);" id="show_howtos" <%if (request("show_howtos")="1") then%>checked <%end if%>/><label for="show_howtos">How Tos</label><br/>
</td><td align="left" valign="middle"><a href="inventory.asp" />Inventory</a></td><td align="right" valign="middle">
<form action="search.asp" method="POST" style="display:inline;">
<input type="text" name="search" size="40" /><input type="submit" value="Search" />
</form>
</td></tr></table>
<table align="left" cellpadding="2" cellspacing="1" class="clienttable">
<%onlyhowtos=((request("show_howtos")="1") and (request("show_clients")="0") and (not(request("show_prospects")="1")))
%>
<tr><th<%if (onlyhowtos) then%> colspan="2"<%end if%>><a href="javascript:sort(1, <%=iif(sortmode=1,(1-sortdir),0)%>);"><%if (onlyhowtos) then%>Title<%else%>Client<%end if%></a></th><%if not(onlyhowtos) then%><th><a href="javascript:sort(2,<%=iif(sortmode=2,(1-sortdir),0)%>);">Job</a></th><%end if%><th><a href="javascript:sort(3,<%=iif(sortmode=3,(1-sortdir),0)%>);">Interests</a></th><th><a href="javascript:sort(4,<%=iif(sortmode=4,(1-sortdir),1)%>);">Date Updated</a></th><th>Edit</th></tr>
<% 
showwhich=""
if not(request("show_clients")="0") then
	showwhich = showwhich & "2,3,5,"
end if
if (request("show_prospects")="1") then
	showwhich = showwhich & "1,"
end if
if (request("show_howtos")="1") then
	showwhich = showwhich & "6,"
end if
if (Len(showwhich)>0) then
	showwhich="and job.job_status_ID IN (" & Left(showwhich, Len(showwhich)-1) & ") "
else
	showwhich="and false"
end if
query= "Select client.client_ID, job.job_ID, client.name, client.state, job.name, job.job_status_ID, job.archive, interests, max(date_updated_1) as date_updated From (Select client.client_ID, job.job_ID, client.name, client.state, job.name, job.job_status_ID, job.archive, min(interest.name) as interests, IIF(max(notes.dateposted) is NULL, job.register_date, max(IIF(notes.dateedited is NULL, notes.dateposted, notes.dateedited))) as date_updated_1 from ((client left join job on client.client_ID=job.client_ID) left join interest on job.job_ID=interest.job_ID) left join notes on job.job_ID=notes.job_ID WHERE archive=0 " & showwhich & " GROUP BY client.client_ID, job.job_ID, client.name, client.state, job.name, job.job_status_ID, job.archive, job.register_date UNION ALL Select client.client_ID, job.job_ID, client.name, client.state, job.name, job.job_status_ID, job.archive, min(interest.name) as interests, IIF(max(task.date_completed) is NULL, job.register_date, max(task.date_completed)) as date_updated_1 from (((client left join job on client.client_ID=job.client_ID) left join interest on job.job_ID=interest.job_ID) left join notes on job.job_ID=notes.job_ID) left join task on task.notes_ID=notes.notes_ID WHERE date_completed is not null and archive=0 " & showwhich & " GROUP BY client.client_ID, job.job_ID, client.name, client.state, job.name, job.job_status_ID, job.archive, job.register_date ) GROUP BY client.client_ID, job.job_ID, client.name, client.state, job.name, job.job_status_ID, job.archive, interests ORDER BY " & sortmodes(sortmode) & " " & IIF(sortdir=1, "DESC", "ASC")
rs.open query,conn
do until rs.eof
	client_id=rs("client_ID")
	job_id=rs("job_ID")
	client_name=rs("client.name")
	job_name=rs("job.name")
	job_status_ID=rs("job_status_ID")
	if (job_status_ID<>6) then if not(isnull(rs("state"))) then client_name = client_name & ", " & UCase(rs("state"))

	rs2.open "Select * from interest where job_ID="&job_id&" ORDER BY name ASC", conn
	i=0
	interests=""
	do until rs2.eof
		if (((i mod 4)=0) and (i<>0)) then interests=interests & "<br/>"
		interests=interests & replace(rs2.Fields.Item("name"), " ", "&nbsp;") & ", "
		i = i + 1
		rs2.MoveNext
	loop
	rs2.close
	if interests="" then
		interests="None"
	else
		interests=Left(interests, Len(interests)-2)
	end if
	%><tr><td<%if (job_status_ID=6) then%> colspan="2"<%end if%>><%=client_name%></td><%if (job_status_ID<>6) then%><td><%=job_name%></td><%end if%><td><%=interests%></td><td><%=FormatDateTime(LocalizeDate(rs("date_updated")), vbShortDate)%></td><td><%
	if (user_rights(27)) then
		response.write "<form action=""register.asp"" method=""POST"" style=""display:inline;""><input type=""hidden"" name=""action"" value=""edit"" /><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Edit"" /></form>"
	end if
	if (user_rights(28)) then
		response.write "<form action=""notes.asp"" method=""GET"" style=""display:inline;""><input type=""hidden"" name=""city"" value="""&job_id&""" /><input type=""Submit"" value=""Notes"" /></form>"
	end if
	%></td></tr><%
	rs.movenext
loop
rs.close
%></table></td><%end if%><td valign="top">
<% 
if (request("tasklist_users_ID")="") then
	tasklist_users_ID=CInt(request.cookies("EPICRISE_user"))
else
	tasklist_users_ID=CInt(request("tasklist_users_ID"))
end if
tasklist_job_ID=request("tasklist_job_ID")
if (request("tasklist_group_tasks")="") then
	tasklist_group_tasks=false
else
	tasklist_group_tasks=(request("tasklist_group_tasks")<>"0")
end if
if (user_rights(35)) then%><%if (not(request("settings_visible")="0")) then%><form action="dashboard.asp" method="POST" name="dashboard_settings_form">
<input type="hidden" name="show_clients" value="<%=request("show_clients")%>" />
<input type="hidden" name="show_prospects" value="<%=request("show_prospects")%>" />
<input type="hidden" name="show_howtos" value="<%=request("show_howtos")%>" />
<input type="hidden" name="sort" value="<%=sortmode%>" />
<input type="hidden" name="dir" value="<%=sortdir%>" />
<input type="hidden" name="menu_visible" value="1" />
<input type="hidden" name="jobs_visible" value="1" />
<input type="hidden" name="tasks_visible" value="1" />
<input type="hidden" name="settings_visible" value="1" />
<input type="hidden" name="fullscreen" value="0" />
<input type="hidden" name="tasklist_job_ID" value="<%=tasklist_job_ID%>" />
<input type="hidden" name="tasklist_group_tasks" value="<%=request("tasklist_group_tasks")%>" />
<select name="tasklist_users_ID" onchange="document.dashboard_settings_form.submit();" style="width:100%;">
  <option value="-1"<%if (tasklist_users_ID=-1) then%> selected<%end if%>>Unassigned Tasks</option>
<% query="Select users.* from users inner join user_level_rights on users.user_level=user_level_rights.user_level_ID WHERE active = 1 and user_level_rights.user_rights_ID=33"
rs.open query, conn
do until rs.eof
%>  <option value="<%=rs("users_ID")%>"<%if (rs("users_ID")=tasklist_users_ID) then%> selected<%end if%>><%=rs("full_name")%></option>
<%rs.movenext
loop
rs.close %></select><br/>
<select name="task_limit" onchange="document.dashboard_settings_form.submit();" style="width:100%;">
<option value="-1">Uncompleted Tasks</option>
<option value="1"<%if (request("task_limit")="1") then%> selected<%end if%>>Yesterday</option>
<option value="2"<%if (request("task_limit")="2") then%> selected<%end if%>>Last Week</option>
<option value="3"<%if (request("task_limit")="3") then%> selected<%end if%>>Last Month</option>
<option value="4"<%if (request("task_limit")="4") then%> selected<%end if%>>All Completed Tasks</option>
</select>
<input type="button" value="Print Tasklist" onclick="printtasks();" /></form><%end if%>
<%end if%>
<table style="Width:100%;">
<% 
if (tasklist_job_ID="") then
	if (tasklist_users_ID=-1) then
		tasklist_title="Unassigned "
	else
		query="Select * from users WHERE users_ID="&tasklist_users_ID
		rs.open query, conn
		tasklist_title=rs("full_name") & "'s "
		rs.close
	end if
else
	if (tasklist_job_ID>0) then
		query="Select client.name, job.name from job inner join client on client.client_ID=job.client_ID WHERE job.job_ID=" & tasklist_job_ID
		rs.open query, conn
		tasklist_title=rs("client.name") & " - " & rs("job.name") & " "
		rs.close
	else
		query = "Select name from client WHERE client_ID=" & (-tasklist_job_ID)
		rs.open query, conn
		tasklist_title=rs("name") & " "
		rs.close
	end if
end if
if (request("task_limit")="1") or (request("task_limit")="2") or (request("task_limit")="3") or (request("task_limit")="4") then
	tasklist_title = tasklist_title & "Completed Tasks"
else
	tasklist_title = tasklist_title & "Tasklist"
end if
%><tr><td><%=tasklist_title%></td></tr><%
date_limit=" and date_completed is NULL"
completed_task=false
query_tables="((task inner join notes on task.notes_ID=notes.notes_ID) inner join job on job.job_ID=notes.job_ID) inner join client on client.client_ID=job.client_ID"
order_by="task.date_due ASC"
select case request("task_limit")
	case "1"
		date_limit=" and date_completed >= #"&DateAdd("d",-1,Date)&"#"
		completed_task=true
	case "2"
		date_limit=" and date_completed >= #"&DateAdd("d",-7,Date)&"#"
		completed_task=true
	case "3"
		date_limit=" and date_completed >= #"&DateAdd("m",-1,Date)&"#"
		completed_task=true
	case "4"
		date_limit=" and date_completed is not null"
		completed_task=true
end select
if (tasklist_job_ID="") then
	search_method="task.users_ID=" & tasklist_users_ID
else
	if (tasklist_job_ID>0) then
		search_method="notes.job_ID=" & tasklist_job_ID
	else
		search_method="job.client_ID=" & (-tasklist_job_ID)
	end if
end if
if (tasklist_group_tasks) then
	order_by="task.users_ID ASC, task.date_due ASC"
end if
query = "Select task_ID, date_completed, date_due, task.users_ID, notes.job_ID, task.notes_ID, summary, client.name from " & query_tables & " WHERE " & search_method & date_limit & " ORDER BY " & order_by
rs.open query, conn
if (rs.eof) then
	query="Select task_ID from task WHERE " & search_method
	rs2.open query, conn
	if ((rs2.eof) or completed_task) then
		%><tr><td style="font-weight:bold;">No tasks found</td></tr><%
	else
		%><tr><td style="font-weight:bold;">All tasks completed</td></tr><%
	end if
	rs2.close
end if
formdata=""
for each item in request.form
	formdata=formdata & "&" & item & "=" & request.form(item)
next
for each item in request.querystring
	formdata=formdata & "&" & item & "=" & request.querystring(item)
next
if (Len(formdata)>0) then
	formdata= "?" & Right(formdata, Len(formdata)-1)
end if
do until rs.eof
	if (tasklist_group_tasks) then
		user_ID=rs("users_ID")
		if (user_ID<>last_user_ID) then
			query="Select full_name FROM users WHERE users_ID=" & user_ID
			rs2.open query, conn
			full_name=rs2("full_name")
			rs2.close
			%><tr><td style="font-size:larger;"><%=full_name%>'s Tasks</td></tr><%
			last_user_ID=user_ID
		end if
	end if
	if (tasklist_group_tasks=false) and (tasklist_job_ID<>"") then
		query="Select full_name FROM users WHERE users_ID=" & rs("users_ID")
		rs2.open query, conn
		full_name=rs2("full_name")
		rs2.close
	end if
	%><tr>
		<td>
			<%if (user_rights(36)) then%>
				<div onmouseover="mouseovertask(<%=rs("task_ID")%>);" onmouseout="mouseouttask(<%=rs("task_ID")%>);">
			<%end if%>
			<div id="taskdate<%=rs("task_ID")%>" style="display:inline;">
				<%if (completed_task) then%>
					<%=rs("date_completed")%>
				<%else%>
					<%=rs("date_due")%>
				<%end if%>
			</div>
			<%if (user_rights(36)) then%>
				<div id="taskdateedit<%=rs("task_ID")%>" style="display:none;">
					<form name="taskdateeditform<%=rs("task_ID")%>" action="dashboard.asp" method="POST" style="display:inline;">
						<input type="hidden" name="tasklist_users_ID" value="<%=request("tasklist_users_ID")%>" />
						<input type="hidden" name="action" value="date_due" />
						<input type="hidden" name="task_ID" value="<%=rs("task_ID")%>" />
						<div style="display:inline;position:relative;">
							<input type="text" size=8 name="date_due" value="<%=rs("date_due")%>" onfocus="calendarShow(this, '_task_<%=rs("task_ID")%>');" readonly />&nbsp;<div id="calendar_task_<%=rs("task_ID")%>" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%curdate=rs("date_due")%><!--#include file="calendar.asp" --></div></div>
							<input type="submit" value="Update" />
						</div>
					</form>
			<%end if%>
			<br/>
			<% if (((rs("users_ID")=CInt(request.cookies("EPICRISE_user"))) and not(CInt(request("task_limit"))>0)) or user_rights(36)) then%>
				<form action="notes.asp" method="POST" name="completetask_<%=rs("task_ID")%>" style="display:inline;">
					<input type="hidden" name="action" value="completetask" />
					<input type="hidden" name="task_ID" value="<%=rs("task_ID")%>" />
					<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
					<input type="checkbox" onclick="document.completetask_<%=rs("task_ID")%>.submit();" />
				</form>
			<%end if%>
			<a href="notes.asp?city=<%=rs("job_ID")%>&note=<%=rs("notes_ID")%>#note_<%=rs("notes_ID")%>">
				<%=rs("name")%>: <%=rs("summary")%>
			</a>
			<%if (tasklist_group_tasks=false) and (tasklist_job_ID<>"") then%>
				- <b><%=full_name%></b>
			<%end if%>
			<div id="edit_task_<%=rs("task_ID")%>" style="display:none;">
				<input type="button" value="Edit" onclick="window.location='edittask.asp?task_ID=<%=rs("task_ID")%>&prevlocation=<%=Server.UrlEncode(Request.ServerVariables("URL"))%><%=Server.UrlEncode(formdata)%>';" />
				<input type="button" value="Move" onclick="window.location='movetask.asp?task_ID=<%=rs("task_ID")%>&prevlocation=<%=Server.UrlEncode(Request.ServerVariables("URL"))%><%=Server.UrlEncode(formdata)%>';" />
			</div>
			<%if (user_rights(36)) then%>
				</div>
			<%end if%>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr><%
	rs.movenext
	' put response buffer flush here
	response.flush()
loop
rs.close
%></table></td></tr></table>
<!--#include file="bodyclose.asp" --></body></html>