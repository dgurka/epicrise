<!--#include file="header.asp" -->
<style type="text/css">
.ntask_list td
{
	border: solid black 2px;
}

.ntask_list
{
	border-collapse: collapse;
}
</style>
<div><a href="dashboard.asp">Back To Dashboard</a></div>
<%

'response.write( WeekDayName( Weekday( "12/20/2012" ) ) )

sql = "SELECT task.date_due AS date_due, client.name AS client_name, task.summary, users.name AS user_name, notes.notes_ID AS note_id, job.job_ID AS job_id FROM (((task LEFT JOIN users ON task.users_ID = users.users_ID) INNER JOIN notes ON task.notes_ID = notes.notes_ID) INNER JOIN job ON notes.job_ID = job.job_ID) INNER JOIN client ON job.client_ID = client.client_ID WHERE (((task.date_completed) Is Null)) ORDER BY task.date_due;"

rs.open sql, conn
%>
<table class="ntask_list">
	<tr>
		<th>Date Due</th>
		<th>Client Name</th>
		<th>Summary</th>
		<th>User Name</th>
	</tr>
<%
do until rs.eof
%>
<tr>
	<td><% response.write(rs("date_due")) %> - <% response.write(WeekDayName(Weekday(rs("date_due")))) %></td>
	<td>
		<a href="notes.asp?city=<% response.write(rs("job_id")) %>">
			<% response.write(rs("client_name")) %>
		</a>
	</td>
	<td style="width: 50%">
		<a href="notes.asp?note=<% response.write(rs("note_id")) %>#note_<% response.write(rs("note_id")) %>">
			<% response.write(rs("summary")) %>
		</a>
	</td>
	<td><% response.write(rs("user_name")) %></td>
</tr>
<%
	rs.movenext
loop
rs.close
%>
</table>