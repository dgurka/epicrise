<!--#include file="header.asp" -->
<%
client_ID = request("client_ID")
query = "SELECT name FROM client WHERE client_ID="&client_ID
rs.open query, conn
name = rs("name")
rs.close
%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Admin Menu</title>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<br/>
<br/>
<br/>

<form method="post">
	<table>
		<tr>
			<td>Customer:</td>
			<td><%= name %></td>
		</tr>
		<tr>
			<td>Start Date:</td>
			<td><input name="start_date" <% if(request.form("start_date")<>"") then %> value="<%= request.form("start_date") %>" <% end if %> type="text"> (optional)</td>
		</tr>
		<tr>
			<td>End Date:</td>
			<td><input name="end_date" <% if(request.form("end_date")<>"") then %> value="<%= request.form("end_date") %>" <% end if %> type="text"> (optional)</td>
		</tr>
		<tr>
			<td><input name="submit" value="submit" type="submit"></td>
			<td></td>
		</tr>
	</table>
</form>

<% if(request.form("submit") <> "") then %>
<style>

.hourstable th, .hourstable td, .hourstable
{
	border: solid black 1px;
	border-collapse: collapse;
}

</style>
<table class="hourstable">
<tr>
	<th>User</th>
	<th>Hours</th>
</tr>
<%

start_date = request.form("start_date")
end_date = request.form("end_date")

where = "client.client_ID = "&client_ID

if(start_date <> "") then
	where = where & " AND notes.date_work >= #"&start_date&"#"
end if

if(end_date <> "") then
	where = where & " AND notes.date_work <= #"&end_date&"#"
end if

query = "SELECT MAX(users.name) as name, Sum(hours) AS [time] FROM ((notes INNER JOIN job ON notes.job_ID = job.job_ID) INNER JOIN client ON job.client_ID = client.client_ID) INNER JOIN users ON notes.users_ID = users.users_ID WHERE "&where&" GROUP BY users.name"

rs.open query, conn
totaltime = 0
do until rs.eof
totaltime = totaltime + rs("time")
%>
<tr>
	<td><%= rs("name") %></td>
	<td><%= rs("time") %></td>
</tr>
<% 
rs.movenext
loop
rs.close 
%>
<tr>
	<td><b>Total</b></td>
	<td><%= totaltime %></td>
</tr>
</table>
<% end if %>

<!--#include file="bodyclose.asp" --></body></html>