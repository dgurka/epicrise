<%
if(request("start")<>"") then
	Response.ContentType = "application/qbooks"
	Response.AddHeader "Content-Disposition", "attachment; filename=time.iif"
end if
%><!--#include file="header.asp" --><%

' reason a why asp classic sucks. No floor method
function floor(x)
    dim temp

    temp = Round(x)

    if temp > x then
        temp = temp - 1
    end if

    floor = temp
end function

if(request("start")<>"") then
	sqlstr = "SELECT note, billable_id, hours, date_work, client.name, users.full_name FROM ((notes INNER JOIN job ON notes.job_ID = job.job_ID) INNER JOIN client ON job.client_ID = client.client_ID) INNER JOIN users ON notes.users_ID = users.users_ID WHERE not notes.hours = 0 AND "
	sqlstr = sqlstr + "date_work >= #"+request("start")+"# AND date_work <= #"+request("end_date")+"#"
	'
	'Response.write("test" + vbTab + "e=nd")
	
	Response.write("!TIMERHDR	VER	REL	COMPANYNAME	IMPORTEDBEFORE	FROMTIMER	COMPANYCREATETIME" + vbCrLf)
	Response.write("TIMERHDR	 8	 0	TEST	N	Y	999278109" + vbCrLf)
	Response.write("!HDR	PROD	VER	REL	IIFVER	DATE	TIME	ACCNTNT	ACCNTNTSPLITTIME" + vbCrLf)
	Response.write("HDR	QuickBooks Pro for Windows	Version 6.0D	Release R12P	1	2013-04-17	1366230157	N	0" + vbCrLf)
	Response.write("!TIMEACT	DATE	JOB	EMP	ITEM	PITEM	DURATION	PROJ	NOTE	BILLINGSTATUS" + vbCrLf)
	Response.flush

	rs.open sqlstr, conn
	do until rs.eof
		line = "TIMEACT" + vbTab
		'line = line + FormatDateTime(rs("date_work"), vbShortDate) + vbTab
		line = line + CStr(Month(rs("date_work"))) + "/" + CStr(Day(rs("date_work"))) + "/" + Replace(CStr(Year(rs("date_work"))), "20", "") + vbTab
		line = line + rs("name") + vbTab
		line = line + rs("full_name") + vbTab
		line = line +  "SPECLABOR" + vbTab + vbTab
		hours = floor(rs("hours")) ' whole part
		dec = rs("hours") - hours
		minutes = dec * 60
		str_minutes = CStr(minutes)
		if(str_minutes = "0") then
			str_minutes = "00"
		end if
		
		line = line + CStr(hours) + ":" + CStr(minutes) + vbTab + vbTab
		'line = line + CStr(rs("hours")) + vbTab
		line = line + Left(Replace(rs("note"), "<br/>", "\n"), 999) + vbTab
		if(rs("billable_id") = 1) then
			line = line + "1"
		else
			line = line + "0"
		end if
		Response.write(line)
		Response.write(vbCrLf)
		Response.flush ' flush after every line so it doesn't overflow
		rs.MoveNext
	loop
	rs.close

	
	Response.end
end if

%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Admin Menu</title>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<br/>
<br/>
<br/>

<form method="post">
	<table>
		<tr>
			<td>Start Date:</td>
			<td><input name="start" type="text"></td>
		</tr>
		<tr>
			<td>End Date:</td>
			<td><input name="end_date" type="text"></td>
		</tr>
		<tr>
			<td><input type="submit"></td>
			<td></td>
		</tr>
	</table>
</form>

<!--#include file="bodyclose.asp" --></body></html>