<!--#include file="header.asp" -->
<!--#include file="freeaspupload.asp" -->
<%if not(user_rights(36)) then server.transfer "defaultpage.asp"
task_ID=cleanInput(request("task_ID"))

if (request.form("action")="edit") then
	summary=cleanInput(request.form("task_summary"))
	task_users_ID=cleanInput(request.form("task_users_ID"))
	task_date_due=cleanInput(request.form("task_date_due"))
	
	query="UPDATE task SET summary='"&summary&"', users_ID="&task_users_ID&", date_due=#"&task_date_due&"#, editor_ID="&Request.Cookies("EPICRISE_user")&", date_edited=NOW() WHERE task_ID="&task_ID
	rs.open query, conn
	
	if (request("prevlocation")<>"") then
		response.redirect request("prevlocation")
	end if
end if
query="Select * from task where task_ID="&task_ID
rs.open query, conn
if (rs.eof) then server.transfer "defaultpage.asp"
%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Edit Task</title>
<script type="text/javascript" src="xmlHttp.js"></script>
<script type="text/javascript">
var xmlHttp;
var calendarEditBox;
var calendarDisplayBox;

function calendarShow(thebox)
{
if (calendarDisplayBox) calendarClose();
calendarEditBox = thebox;
calendarDisplayBox = document.getElementById("calendar");
calendarDisplayBox.style.display="block";
calendarEditBox.blur();
calendarDisplayBox.focus();
}

function calendarClose()
{
calendarDisplayBox.style.display="none";
}

function calendarSelect(month, day, year)
{
calendarDisplayBox.style.display="none";
calendarEditBox.value=month+"/"+day+"/"+year;
calendarMove(calendarEditBox.value);
}

function calendarUpdate()
{
if (xmlHttp.readyState==4)
{
calendarDisplayBox.innerHTML=xmlHttp.responseText;
}
}

function calendarMove(newdate)
{
xmlHttp=createXmlHttp();
xmlHttp.onreadystatechange=calendarUpdate;
xmlHttp.open("GET","calendar.asp?curdate="+escape(newdate),true);
xmlHttp.send(null);
}
</script>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<form action="edittask.asp" method="POST">
<input type="hidden" name="task_ID" value="<%=task_ID%>" />
<input type="hidden" name="action" value="edit" />
<input type="hidden" name="prevlocation" value="<%=request("prevlocation")%>" />
<table>
<tr><td colspan="2">Edit Task:</td></tr>
<tr align="left"><td colspan="2"><input type="text" name="task_summary" value="<%=Replace(rs("summary"),"""","&quot;")%>" style="width:400px;" /></td></tr>
<tr align="left"><td><select name="task_users_ID"><option value="-1">Unassigned</option>
<% query="Select users.* from users inner join user_level_rights on users.user_level=user_level_rights.user_level_ID WHERE user_level_rights.user_rights_ID=33"
rs1.open query, conn
do until rs1.eof
%>  <option value="<%=rs1("users_ID")%>"<%if (rs("users_ID")=rs1("users_ID")) then%> selected<%end if%>><%=rs1("full_name")%></option>
<%rs1.movenext
loop
rs1.close %>
</select></td>
<td>Due Date:&nbsp;<div style="display:inline;position:relative;"><input type="text" size=8 name="task_date_due" value="<%=rs("date_due")%>" onfocus="calendarShow(this);" readonly />&nbsp;<div id="calendar" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%curdate=rs("date_due")%><!--#include file="calendar.asp" --></div></div></td></tr>
<tr><td colspan="2" align="center"><input type="submit" value="Update" /></td></tr></table>
</form>
<!--#include file="bodyclose.asp" --></body></html>