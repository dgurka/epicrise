<!--#include file="header.asp" -->
<!--#include file="freeaspupload.asp" -->
<%

if not(user_rights(28)) then response.redirect "defaultpage.asp"
if (request.querystring("job_ID")<>"") then
	job_ID=CInt(request.querystring("job_ID"))
	prevlocation=request.queryString("prevlocation")
elseif (request.queryString("city")<>"") then
	job_ID=CInt(request.queryString("city"))
	prevlocation=request.queryString("prevlocation")
elseif (request.querystring("upload")<>1) then
	prevlocation=request.form("prevlocation")
	job_ID=CInt(request.form("city"))
end if
if (request.queryString("note")<>"") then
	query="Select job_ID from notes WHERE notes_ID=" & request.queryString("note")
	rs.open query, conn
	if (not(rs.eof)) then
		job_ID=CInt(rs("job_ID"))
	end if
	rs.close()
end if

grouptasks=true

if (request.QueryString("upload")=1) then
	dim Upload
	Set Upload = New FreeASPUpload
	Upload.Save(Server.MapPath("/epicrise/db/inventory/"))
	ks=Upload.UploadedFiles.keys
	prevlocation=Upload.Form("prevlocation")
	job_ID=Upload.Form("city")
	
	if (Upload.form("action")="grouptasks") and (Upload.form("grouptasks")<>"yes") then
		grouptasks=false
	end if
	if (Upload.form("grouptasks")="no") then
		grouptasks=false
	end if
	
	
	if (Upload.form("action")="post") and (user_rights(29)) then
		if (Upload.form("hours")<>"") then
			hours=cleanInput(Upload.form("hours"))
		else
			hours = 0
		end if
		note=cleanInput(Upload.form("note"))
		billable=Upload.form("billable")
		date_work=Upload.form("date_work")
		nextcall=Upload.form("nextcall")
		if (user_rights(23)) then
			query="INSERT INTO notes(job_ID, users_ID, dateposted, `note`, hours, billable_ID, date_work, approved_ID) VALUES('"&job_ID&"', '"&user&"', Now(), '"&note&"', '"&hours&"', '"&billable&"', #"&date_work&"#, 1)"
		else
			query="INSERT INTO notes(job_ID, users_ID, dateposted, `note`) VALUES('"&job_ID&"', '"&user&"', Now(), '"&note&"')"
		end if
		
		rs.open query, conn
		rs.open "SELECT @@IDENTITY"
		notes_ID=rs(0)
		rs.close
		if (Upload.form("task_summary_blank")<>"1") and (Upload.form("task_summary_blank")<>"") then
			task_users_ID=CInt(Upload.form("task_users_ID"))
			task_summary=cleanInput(Upload.form("task_summary"))
			task_date_due=cleanInput(Upload.form("task_date_due"))
			query="INSERT INTO task(notes_ID, users_ID, summary, date_assigned, date_due) VALUES("&notes_ID&", "&task_users_ID&", '"&task_summary&"', NOW(), '"&task_date_due&"')"
			rs.open query, conn
		end if
		
		query="Delete from call WHERE job_ID="&job_ID&" and users_ID="&Request.Cookies("EPICRISE_user")
		rs.open query,conn
		if (IsDate(nextcall)) then
			nextcall=FormatDateTime(CDate(nextcall), 0)
			query="Insert Into call(job_ID, users_ID, calldate) Values("&job_ID&", "&Request.Cookies("EPICRISE_user")&", #"&nextcall&"#)"
			rs.open query,conn
		end if
		
		i=1
		for each filekey in ks
			path=cleanInput(Upload.UploadedFiles(filekey).FileName)
			file_name = cleanInput(Upload.UploadedFiles(filekey).OrigFileName)
			name=cleanInput(Upload.Form("attachment_name_" & CStr(i)))
			query="Insert into inventory(`path`, `file_name`, `name`, `notes_ID`, `users_ID`, `date_edited`) Values('"&path&"', '"&file_name&"', '"&name&"', "&notes_ID&", "&Request.Cookies("EPICRISE_user")&", NOW())"
			rs.open query, conn
			i = i + 1
		next
		
		response.redirect "notes.asp?city="&job_ID&"&prevlocation="&prevlocation
	else
		response.write ("UPLOAD FAILED - FILE TOO LARGE")
		response.end
	end if
else
	if (request.form("action")="grouptasks") and (request.form("grouptasks")<>"yes") then
		grouptasks=false
	end if
	if (request.form("grouptasks")="no") then
		grouptasks=false
	end if
end if

if (request.form("action")<>"edit") and (request.form("action")<>"delete") and (request.form("action")<>"completetask") and (job_ID>0) then
	rs.open "Select * from job where job_ID="&job_ID, conn
	client_ID=rs("client_ID")
	name=rs("name")
	rs.close
	rs.open "Select * from client WHERE client_ID="&client_ID, conn
	city=rs("name")
	state=rs("state")
	rs.close
	
	query="Select * from call where job_ID="&job_ID&" and users_ID="&Request.Cookies("EPICRISE_user")
	rs.open query,conn
	if (rs.eof) then
		nextcall=""
	else
		nextcall=rs("calldate")
	end if
	rs.close
end if

if (job_ID<0) then
	rs.open "Select * from client WHERE client_ID="&(-job_ID), conn
	city=rs("name")
	state=rs("state")
	rs.close
end if

if (request.form("action")="edit") then
	if (request.form("hours")<>"") then
		hours=cleanInput(request.form("hours"))
	else
		hours = 0
	end if
	notes_ID=request.form("notes_ID")
	query="Select * from notes WHERE notes_ID="&notes_ID
	rs.open query, conn
	users_ID=rs("users_ID")
	job_ID=rs("job_ID")
	approved_ID=rs("approved_ID")
	rs.close
	if (((users_ID=CInt(request.cookies("EPICRISE_user"))) and (user_rights(21))) or (user_rights(22))) then 
		note=cleanInput(request.form("note"))
		billable=request.form("billable")
		date_work=request.form("date_work")
		query="UPDATE notes SET "
		if (user_rights(23)) then
			query = query & "hours="&hours&", billable_ID="&billable&", date_work=#"&date_work&"#, "
		end if
		query = query & "approved_ID=1, `note`='" & note & "', editor_ID="&request.cookies("EPICRISE_user")&", dateedited=NOW() WHERE notes_ID="&notes_ID
		rs.open query, conn
		if (approved_ID<>1) then
			query = "Select users.email from user_notification inner join users on user_notification.users_ID=users.users_ID WHERE notification_ID=1"
			rs.open query , conn
			do until rs.eof
				Set objCDOMail = Server.CreateObject("CDO.Message")
				objCDOMail.From = "no-reply@enablepoint.com"
				objCDOMail.To = rs("email")
				objCDOMail.Subject = "EPICRISE Approved Note Edited"
				objCDOMail.HTMLBody = name & " has updated a note that was previously marked approved.<br/><br/><a href=""http://66.241.199.124/epicrise/notes.asp?note=" & notes_ID & """>Click here to view the note.</a>"
					
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="mail52.safesecureweb.com"
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False 'Use SSL for the connection (True or False)
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

				objCDOMail.Configuration.Fields.Update
				objCDOMail.Send

				Set objCDOMail = Nothing 
				rs.movenext
			loop
			rs.close
		end if
	end if
	response.redirect "notes.asp?note=" & notes_ID & "&prevlocation="&prevlocation & "#note" & notes_ID
elseif (request.form("action")="delete") then
	notes_ID=request.form("notes_ID")
	query="Select * from notes WHERE notes_ID="&notes_ID
	rs.open query, conn
	users_ID=rs("users_ID")
	job_ID=rs("job_ID")
	approve_ID=rs("approved_ID")
	rs.close
	if (((users_ID=CInt(request.cookies("EPICRISE_user"))) and (user_rights(21))) or (user_rights(22))) then 
		query="DELETE FROM task WHERE notes_ID="&notes_ID
		rs.open query, conn
		query="DELETE FROM notes WHERE notes_ID="&notes_ID
		rs.open query, conn
	end if
		if (approve_ID<>1) then
			query = "Select users.email from user_notification inner join users on user_notification.users_ID=users.users_ID WHERE notification_ID=1"
			rs.open query , conn
			do until rs.eof
				Set objCDOMail = Server.CreateObject("CDO.Message")
				objCDOMail.From = "no-reply@enablepoint.com"
				objCDOMail.To = rs("email")
				objCDOMail.Subject = "EPICRISE Approved Note Deleted"
				objCDOMail.HTMLBody = name & " has deleted a note that was previously marked approved.<br/><br/><a href=""http://66.241.199.124/epicrise/notes.asp?city=" & job_ID & """>Click here to view the job.</a>"
					
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="relay-hosting.secureserver.net"
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False 'Use SSL for the connection (True or False)
				objCDOMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

				objCDOMail.Configuration.Fields.Update
				objCDOMail.Send

				Set objCDOMail = Nothing
				rs.movenext
			loop
			rs.close
		end if
	response.redirect "notes.asp?city="&job_ID&"&prevlocation="&prevlocation
elseif (request.form("action")="completetask") then
	task_ID=request.form("task_ID")
	query="Select * from task WHERE task_ID="&task_ID & " and date_completed IS NULL"
	rs.open query, conn
	if (rs.eof) then response.redirect "defaultpage.asp"
	users_ID=rs("users_ID")
	query="Select * from notes WHERE notes_ID="&rs("notes_ID")
	rs.close
	rs.open query, conn
	job_ID=rs("job_ID")
	rs.close
	if ((users_ID=CInt(request.cookies("EPICRISE_user")) or users_ID=-1) and (user_rights(33)) or user_rights(36)) then 
		query="UPDATE task SET date_completed=NOW()"
		if (users_ID=-1) then query = query & ", users_ID="&request.cookies("EPICRISE_user")
		query = query & " WHERE task_ID="&task_ID
		rs.open query, conn
	end if
	response.redirect "notes.asp?city="&job_ID&"&prevlocation="&prevlocation
elseif (request.form("action")="approve") and (user_rights(39)) then
	if (isNumeric(request.form("notes_ID"))) then
		query="UPDATE notes SET approved_ID=2 WHERE notes_ID=" & request.form("notes_ID")
		rs.open query, conn
	end if
end if



page=1
if ((IsNumeric(request("page"))) and request("page")<>"") then
	page=CInt(request("page"))
end if

%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title><%=city%><%if (state<>"") then%>, <%=state%><%end if%><%=chr(32)%><%=name%> - Notes</title>
<style>
table.notestable {
	background-color: black;
}
table.notestable tr {
	background-color: white;
}
table.notestable td {
	vertical-align: top;
	padding: 10px;
}
.editbox
{
	float:right;
}
</style>
<script type="text/javascript" src="xmlHttp.js"></script>
<script type="text/javascript">
function editnote(note) {
	notesbox=document.getElementById("notesbox"+note);
	notes=document.getElementById("notes"+note).innerHTML;
	noteseditbox=document.getElementById("noteseditbox"+note);
	edittext=document.getElementById("noteseditform"+note).innerHTML;
	notesbox.innerHTML=edittext;//'<form action="notes.asp" method="POST"><input type="hidden" name="action" value="edit"><input type="hidden" name="city" value="<%=city%>" /><input type="hidden" name="notes_ID" value="'+note+'" /><input type="hidden" name="prevlocation" value="<%=prevlocation%>" /><textarea name="note" style="width:100%;height:200px;">'+notes+'</textarea><input type="submit" value="Update" /></form>';
}

function deletenote(note) {
	if (confirm("Are you sure you want to delete this note?\n\nDeleting a note is permanent!")) {
		note.submit();
	}
	return;
}

function page(pagenum)
{
	document.page.page.value=pagenum;
	document.page.submit();
}

function validatenumber(elem) {
var myval;
myval=Math.abs(elem.value);
if (isNaN(myval)) {
myval=0;
} else {
myval=myval*4;
myval=Math.ceil(myval);
myval=myval/4;
}
myval=myval.toFixed(2);
elem.value=myval;
}

document.onclick=function(event){docclick(event)};
var allowclose=true;
function docclick(event)
{
	if (allowclose)
	{
		var ev = window.event ? window.event : event;
		var obj = ev.target ? ev.target : ev.srcElement;
		while (obj)
		{
			if (obj.getAttribute)
				if ((obj.getAttribute("name")=="calendar_display") | (obj.getAttribute("ID")=="date_work"))
					return;
			obj=obj.parentNode;
		}
		var cals = document.getElementsByTagName("div");
		var i = 0;
		while (i<cals.length)
		{
			if (cals[i].getAttribute("name")=="calendar_display")
				cals[i].style.display='none';
			i++;
		} 
	}
}

var xmlHttp;
var calendarEditBox;
var calendarDisplayBox;

function calendarShow(thebox, note_ID)
{
if (calendarDisplayBox) calendarClose();
calendarEditBox = thebox;
calendarDisplayBox = document.getElementById("calendar" + note_ID);
calendarDisplayBox.style.display="block";
calendarEditBox.blur();
calendarDisplayBox.focus();
allowclose=false;
setTimeout('allowclose=true;', 100);
}

function calendarClose()
{
calendarDisplayBox.style.display="none";
}

function calendarSelect(month, day, year)
{
calendarDisplayBox.style.display="none";
calendarEditBox.value=month+"/"+day+"/"+year;
calendarMove(calendarEditBox.value);
}

function calendarUpdate()
{
if (xmlHttp.readyState==4)
{
calendarDisplayBox.innerHTML=xmlHttp.responseText;
}
}

function calendarMove(newdate)
{
xmlHttp=createXmlHttp();
xmlHttp.onreadystatechange=calendarUpdate;
xmlHttp.open("GET","calendar.asp?curdate="+escape(newdate),true);
xmlHttp.send(null);
}

function task_summary_focus()
{
	var blankitem = document.addnoteform.task_summary_blank;
	var summary = document.addnoteform.task_summary;
	if (blankitem.value=='1') {
		blankitem.value='0';
		summary.value='';
		summary.style.color='black';
	}
}

function task_summary_blur()
{
	var blankitem = document.addnoteform.task_summary_blank;
	var summary = document.addnoteform.task_summary;
	if (summary.value=='') {
		blankitem.value='1';
		summary.value='Summary';
		summary.style.color='gray';
	}
}

function validate_form()
{
	if (document.addnoteform.task_summary_blank.value=='0') {
		if (document.addnoteform.task_users_ID.value=='-1') {
			alert('You entered a task but did not select a user');
			return false;
		}
	}
	return true;
}

var attachment_num = 1;

function add_attachment()
{
	var my_div = document.getElementById('attachments');
	el = document.createElement("input");
	el = my_div.appendChild(el);
	el.name = "attachment_name_"+attachment_num;
	el.type = "text";
	el.style.width="100px";
	
	el2 = document.createElement("input");
	el2.type = "file";
	el2.name = "path"+attachment_num;
	el2 = my_div.appendChild(el2);
	
	my_div.appendChild(document.createElement("br"));
	
	attachment_num = attachment_num + 1;
}

function disablebutton(btn) {
	btn.disabled=true;
	btn.value="Uploading...";
	document.addnoteform.submit();
}

<%if (user_rights(36)) then%>
var tasktimer;
var taskhidetimer;

function mouseovertask(task)
{
	tasktimer = setTimeout('showedittask('+task+');',500);
}

function mouseouttask(task)
{
	clearTimeout(tasktimer);
	taskhidetimer = setTimeout('hideedittask('+task+');',5000);
}

function showedittask(task)
{
	document.getElementById('edit_task_'+task).style.display='block';
}

function hideedittask(task)
{
	document.getElementById('edit_task_'+task).style.display='none';
}<%end if%><%if (user_rights(39)) then%>

function approvenote(notes_ID)
{
	document.approveform.notes_ID.value = notes_ID;
	document.cookie = "scrollLeft=" + document.body.scrollLeft + ";scrollTop=" + document.body.scrollTop;
	document.approveform.submit();
}<%end if%>

function init()
{
	<%
	if (request.QueryString("note")<>"") then
		note_ID=request.querystring("note")
	%>	shownote(<%=note_ID%>);
<%end if%>
	<%
	if (request.form("action")="approve") then%>
	shownote(<%=request.form("notes_ID")%>);
	<%end if%>
}

function shownote(note)
{
	var noteboxes = document.getElementsByTagName('table');
	var cells;
	x=0;
	do
	{
		if (noteboxes[x].id.substring(0,7)=="notebox")
		{
			noteboxes[x].className = noteboxes[x].className.replace(/selected/g, "");
		}
		x++;
	} while (x<noteboxes.length);
	
	var notebox = document.getElementById('notebox_'+note)
	if (notebox)
	{
		<%if (request.form("action")<>"approve") then%>
		notebox.className += " selected";
		<%end if%>
		window.location='#note_'+note;
	} else {
		window.location="notes.asp?city=<%=job_ID%>&note=" + note + "#note_" + note;
	}
}

var pageTitle = "<%=Replace(city, """", "\""")%><%if (state<>"") then%>, <%=Replace(state, """", "\""")%><%end if%><%=chr(32)%><%=Replace(name, """", "\""")%>";

function printnote(note) {
	shownote(note);
	window.print();
}
</script>
</head>
<body onload="init();"><%if (user_rights(27)) then%>
<form action="register.asp" method="POST" style="display:none;" name="editform"><input type="hidden" name="action" value="edit" /><input type="hidden" name="city" value="<%=job_id%>" /></form><%end if%>
<!--#include file="menu.asp" -->
<!--#include file="bodyopen.asp" -->
<table class="navigation"><tr><td><span style="background-color: yellow;font-size: large;">&nbsp;<%=city%><%if (state<>"") then%>, <%=state%><%end if%>
<%
query = "Select * from client WHERE client_ID=" & client_ID
rs.open query, conn
inactive=false
if(rs("inactive")=1) then
	inactive=true
end if
rs.close
%>
<%if (job_ID>0) then
	query = "Select * from job WHERE job_ID=" & job_ID
	rs.open query, conn
	job_status_ID=rs("job_status_ID")
	rs.close
	query = "Select * from job WHERE client_ID="&client_ID&" ORDER BY name ASC"
else
	query = "Select * from job WHERE client_ID="&(-job_ID)&" ORDER BY name ASC"
	job_status_ID=-1
end if
%><%if (job_status_ID<>6) then %> - <form name="jobform" action="notes.asp" method="POST" style="display: inline;"><select name="city" onchange="document.jobform.submit();" style="background-color: yellow;"><option value="-<%=client_ID%>">All Jobs</option><%
rs.open query, conn
do until rs.eof
%><option value="<%=rs("job_ID")%>"<%if (CStr(rs("job_ID"))=CStr(job_ID)) then%> selected<%end if%>><%=rs("name")%></option>
<%
rs.movenext
loop
rs.close
%></select></form><%end if%></span> &nbsp; <a href="inventory.asp?cust=<%=job_ID%>">Inventory</a> <a href="info.asp?clid=<%= client_ID %>">Info</a></td><td align="right" valign="middle"><form action="search.asp" method="POST" style="display:inline;"><input type="hidden" name="job_id" value="<%=job_ID%>" /><input type="text" name="search" size="40" /><input type="submit" value="Search" /></form></td></tr></table>
<!--<input type="button" value="Back to <%if (prevlocation="1") then %>Call List<%else%>Pipeline<%end if%>" onclick="window.location='<%if (prevlocation="1") then %>calllist.asp<%else%>pipeline.asp<%end if%>';" />-->
<%if (user_rights(29)) and (job_ID>0) then%><form action="notes.asp?upload=1" method="POST" name="addnoteform" enctype="multipart/form-data" onsubmit="return validate_form();">
<input type="hidden" name="city" value="<%=job_ID%>" />
<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
<input type="hidden" name="action" value="post" />
<%if (user_rights(23)) or (user_rights(20)) or (user_rights(34)) then%><table style="width: 900px;"><tr><%if (user_rights(23)) then%><td><table><tr><td>Hours</td><td> <input type="text" name="hours" onchange="validatenumber(this);" /></td></tr>
<tr><td>Billable:</td><td><select name="billable">
<%query="Select * from billable order by rank ASC"
rs.open query,conn
do until rs.eof
%><option value="<%=rs("billable_ID")%>"><%=rs("name")%></option>
<%
rs.movenext
loop
rs.close
%></select></td></tr>
<tr><td>Work Date</td><td><div style="display:inline;position:relative;"><input type="text" size=8 name="date_work" value="<%if (edit) then response.write date_work else response.write FormatDateTime(LocalizeDate(Date), vbShortDate)%>" onfocus="calendarShow(this, '');" readonly />&nbsp;<div id="calendar" name="calendar_display" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%if (edit) then curdate=date_work else curdate=Date%><!--#include file="calendar.asp" --></div></div></td></tr>
</table></td><%end if%><%if ((user_rights(20)) or (user_rights(27))) then%><td align="center" valign="bottom" style="padding-bottom:5px;">
<table width="100%" height="100%"><%if (user_rights(27)) then%><tr><td valign="top"><a href="timereport2.asp?client_ID=<%= client_ID %>">Hours Report</a></td></tr><tr>
<td valign="top">
<input type="button" onclick="document.editform.submit();" value="Edit Job" />
</td></tr><%end if%><%if (user_rights(20)) then%><tr><td>
Next Call:&nbsp;<div style="display:inline;position:relative;"><input type="text" size=8 name="nextcall" value="<%=nextcall%>" onfocus="calendarShow(this, '_nextcall');" readonly />&nbsp;<input type="button" value="Clear" onclick="document.addnoteform.nextcall.value='';" /><div id="calendar_nextcall" name="calendar_display" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%curdate=nextcall%><!--#include file="calendar.asp" --></div></div>
</td></tr><%end if%></table></td>
<%end if%><% if (user_rights(34)) then %><td align="right">
<table><tr align="left"><td colspan="2">Assign a Task:</td></tr>
<tr align="left"><td colspan="2"><input type="hidden" name="task_summary_blank" value="1" /><input type="text" name="task_summary" value="Summary" style="width:400px; color: gray;" onfocus="task_summary_focus();" onblur="task_summary_blur();" /></td></tr>
<tr align="left"><td><select name="task_users_ID"><option value="-1">Select a user</option>
<% query="Select users.users_ID, users.full_name from users inner join user_level_rights on users.user_level=user_level_rights.user_level_ID WHERE active = 1 AND user_level_rights.user_rights_ID=33"
rs.open query, conn
do until rs.eof
%>  <option value="<%=rs("users_ID")%>"><%=rs("full_name")%></option>
<%rs.movenext
loop
rs.close %>
</select></td>
<td>Due Date:&nbsp;<div style="display:inline;position:relative;"><input type="text" size=8 name="task_date_due" value="<%=FormatDateTime(LocalizeDate(Date), vbShortDate)%>" onfocus="calendarShow(this, '_task');" readonly />&nbsp;<div id="calendar_task" name="calendar_display" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%curdate=Date%><!--#include file="calendar.asp" --></div></div></td></tr>
</table></td><% end if%></tr></table><%
else
%>
<%end if%>
<textarea name="note" style="width:900px; height:200px;"></textarea><br/>
<div id="attachments">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File<br/></div>
<% if(inactive) then %>
<input type="button" value="Post" onclick="alert('This account has been de-activated. Please ask your administrator to enable.');return false;" />
<% else %>
<input type="button" value="Post" onclick="disablebutton(this);" />
<% end if %>
<input type="button" onclick="add_attachment();" name="addattachment" value="Add Attachment" />
<input type="hidden" name="grouptasks" value="<%if (grouptasks) then %>yes<%else%>no<%end if%>" />
</form><%end if%><% 
if (job_ID>0) then
	query="Select * from (task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID WHERE notes.job_ID="&job_ID&" and date_completed is NULL"
else
	query="Select * from ((task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID) inner join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and date_completed is NULL"
end if
rs.open query, conn
if (rs.eof) then tasksexist=false else tasksexist=true
rs.close
'if (job_status_ID=6) then
'	ordermode="ASC"
'	oppositeorder="DESC"
'else
	ordermode="DESC"
	oppositeorder="ASC"
'end if
if (job_ID>0) then
	query = "Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes left outer join task on notes.notes_ID=-task.task_ID WHERE notes.job_ID="&job_ID& _ 
	vbcrlf & " UNION ALL " & vbcrlf & _
	"Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from notes right outer join task on notes.notes_ID=task.notes_ID WHERE notes.job_ID="&job_ID&" and task.date_completed is not null ORDER BY datesort " & ordermode
else
	query = "Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from (notes left outer join task on notes.notes_ID=-task.task_ID) left join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)& _ 
	vbcrlf & " UNION ALL " & vbcrlf & _
	"Select *, IIF(task.task_ID is null, dateposted, date_completed) as datesort from (notes right outer join task on notes.notes_ID=task.notes_ID) left join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and task.date_completed is not null ORDER BY datesort " & ordermode
end if
countquery = "SELECT count(notes.notes_ID) as TOTAL FROM ( " & query & ")"
rs.open countquery, conn
totalitems = rs("TOTAL")
rs.close
totalpages = -Int(-totalitems/ITEMS_PER_PAGE)
if (request.QueryString("note")<>"") then
	rs.open query, conn
	my_note_ID=request.queryString("note")
	i = 0 
	my_not_pos=-1
	do until rs.eof
		i = i + 1
		if ((rs("notes.notes_ID")=CInt(my_note_ID)) and (isNull(rs("task_ID")))) then
			my_not_pos = i
			exit do
		end if
		rs.movenext
	loop
	rs.close
	if (my_not_pos<>-1) then
		page = -Int(-my_not_pos/ITEMS_PER_PAGE)
	end if
end if
if (totalitems>ITEMS_PER_PAGE) then
	query = "SELECT TOP " & page*ITEMS_PER_PAGE & " * FROM ( " & query & ") ORDER BY datesort " & ordermode
	if (page<>1) then
		query = "SELECT * FROM ( SELECT TOP " & ITEMS_PER_PAGE & " *  FROM ( " & query & " ) ORDER BY datesort " & oppositeorder & ") ORDER BY datesort " & ordermode
	end if
end if
%><form action="notes.asp" method="POST" name="page" style="display:none;">
<input type="hidden" name="city" value="<%=job_ID%>" />
<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="grouptasks" value="<%if (grouptasks) then %>yes<%end if%>" />
</form><%if (user_rights(39)) then%>
<form action="notes.asp" method="POST" name="approveform" style="display:none;">
<input type="hidden" name="city" value="<%=job_ID%>" />
<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
<input type="hidden" name="page" value="<%=page%>" />
<input type="hidden" name="grouptasks" value="<%if (grouptasks) then %>yes<%end if%>" />
<input type="hidden" name="action" value="approve" />
<input type="hidden" name="notes_ID" value="" />
</form><%end if%>
<table class="pages"><tr><td align="center">
Page:<% 
if (page<>1) then
	%>&nbsp;<a href="javascript:page(1)"><%if (ordermode="ASC") then%>Oldest<%else%>Newest<%end if%></a>&nbsp;<a href="javascript:page(<%=i-1%>)"><%if (ordermode="ASC") then%>Older<%else%>Newer<%end if%></a><%
end if
for i = 1 to totalpages
	if (i=page) then
		%>&nbsp;<b><%=i%></b><%
	else
		%>&nbsp;<a href="javascript:page(<%=i%>)"><%=i%></a><%
	end if
next
if (page<>totalpages) then
	%>&nbsp;<a href="javascript:page(<%=page+1%>)"><%if (ordermode="ASC") then%>Newer<%else%>Older<%end if%></a>&nbsp;<a href="javascript:page(<%=totalpages%>)"><%if (ordermode="ASC") then%>Newest<%else%>Oldest<%end if%></a><%
end if
%></td></tr></table>
<table cellpadding="0" cellspacing="0" class="workflow <%if (tasksexist) then response.write("tasksexist") %>"><tr><td valign="top" class="notes"><%
page_query=query
rs.open query, conn
if (rs.eof) then
%><table class="notestable" cellpadding="0" cellspacing="1"><tr><td style="text-align: center;font-weight:bold;">No Notes Posted</td></tr></table><%
end if
set re = new RegExp
re.pattern = "(((http://)|(https://))((([a-zA-Z0-9]+\.)?[a-zA-Z0-9]+\.[a-zA-Z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))[a-zA-Z0-9\-_/\\]*\.?[a-zA-Z0-9\-_#=\?\&_/\\\.~\%\!]+)"
re.global = True
set re2 = new RegExp
re2.pattern = "^(?!http://)(?!https://)(((([a-zA-Z0-9]+\.)?[a-zA-Z0-9]+\.[a-zA-Z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))[a-zA-Z0-9/\\]*\.?[a-zA-Z0-9#=\?\&_/\\\.~\%\!]+)"
re2.global = True
do until rs.eof
	if (isnull(rs("task_ID"))) then
		notes_id=rs("notes.notes_ID")
		users_ID=rs("notes.users_ID")
		dateposted=LocalizeDate(CDate(rs("dateposted")))
		note=rs("note")
		if (job_ID<=0) then job_name = rs("name")
		editnotetext = note
		note = re.replace(note, "<a href=""$1"">$1</a>")
		note = re2.replace(note, "<a href=""http://$1"">$1</a>")
		if (isNull(rs("hours"))) then hours=0 else hours=rs("hours")
		if (isNull(rs("date_work"))) then date_work="" else date_work=CDate(rs("date_work"))
		if (isNull(rs("billable_ID"))) then billable_ID="" else billable_ID=rs("billable_ID")
		if (isNull(rs("approved_ID"))) then approved_ID="" else approved_ID=rs("approved_ID")
		dateedited=LocalizeDate(rs("dateedited"))
		editor_ID=rs("notes.editor_ID")
		notes_ID=rs("notes.notes_ID")
		query="Select * from `users` WHERE users_ID="&users_ID
		rs2.open query, conn
		if (rs2.eof) then
			username=STR_USER_NOT_FOUND
			user_level_ID=-1
		else
			username=rs2("full_name")
			user_level_ID=rs2("user_level")
		end if
		rs2.close
		query="Select * from billable WHERE billable_ID="&billable_ID
		rs2.open query, conn
		if (rs2.eof) then
			billable=UNDEFINED_VAR
		else
			billable=rs2("name")
		end if
		rs2.close
		if (approved_ID<>"") then
			query="Select * from approved WHERE approved_ID="&approved_ID
			rs2.open query, conn
			if (rs2.eof) then
				approved=UNDEFINED_VAR
			else
				approved=rs2("name")
			end if
			rs2.close
		end if
		if (editor_id<>"") then
			query="Select * from `users` WHERE users_ID="&editor_ID
			rs2.open query, conn
			if (rs2.eof) then
				editorname=STR_USER_NOT_FOUND
			else
				editorname=rs2("full_name")
			end if
			rs2.close
		end if
		query="Select * from user_level WHERE user_level_ID="&user_level_ID
		rs2.open query, conn
		if (rs2.eof) then
			user_level=""
		else
			user_level=rs2("name")
		end if
		rs2.close
%><a name="note_<%=notes_ID%>"><table class="notestable" id="notebox_<%=notes_ID%>" cellpadding="0" cellspacing="1"><tr><td class="metadata">
	<div style="float:right;font-size:smaller;">#<%=notes_ID%></div>
	<div style="font-size:larger;"><%=username%></div>
	<div style="font-size:smaller;"><%=user_level%><br/>
	<%=dateposted%><%if (hours<>0) then
	%><br/><br/>Hours: <%=FormatNumber(hours,2)%><br/>
	<%=billable%> - <b class="approved<%=approved_ID%>"><%if (user_rights(39)) and (approved_ID=1) then%><a href="javascript:approvenote('<%=notes_ID%>');"><%=approved%></a><%else%><%=approved%><%end if%></b><%if (Year(date_work)<>Year(dateposted)) or (Month(date_work)<>Month(dateposted)) or (Day(date_work)<>Day(dateposted)) then
	%><br/>Work Date: <%=date_work%><%end if%><%end if%><% if (editor_ID<>"") then
	%><br/><br/>Last edited by <%=editorname%> at<br/>
	<%=dateedited%>
	<% end if %></div>
	</td><td id="notesbox<%=notes_ID%>" class="body"><div class="body">
	<%if (job_ID<=0) then%><%=city%><%if (state<>"") then%>, <%=state%><%end if%><%=" "%><%=job_name%>:<hr width="100%"/><%end if%>
	<div id="noteseditbox<%=notes_ID%>" class="editbox">
		<a href="javascript:printnote(<%=notes_ID%>);">Print</a>
		<%if (((users_ID=CInt(request.cookies("EPICRISE_user"))) and (user_rights(21))) or (user_rights(22))) then %>
			<a href="javascript:editnote(<%=notes_ID%>);">Edit</a> 
			<a href="javascript:deletenote(document.deletenoteform<%=notes_ID%>);">Delete</a>
		<%end if%>
	</div>
	<div><%=note%></div><%
	query = "Select * from inventory WHERE notes_ID="&notes_ID
	rs2.open query, conn
	if not(rs2.eof) then 
		%><hr/><table style="width:100%;text-align:center;table-layout:fixed;"><tr><%
		count=0
		do until rs2.eof
		count=count+1
		%><td><a href="download.asp?file=<%=rs2("inventory_ID")%>"><%if ((LCase(Right(rs2("path"),3))="jpg") or (LCase(Right(rs2("path"),3))="gif") or (LCase(Right(rs2("path"),3))="png") or (LCase(Right(rs2("path"),4))="jpeg")) then%><img src="db/inventory/<%=rs2("path")%>" border="0" style="<%if (LCase(Right(rs2("path"),3))="png") then%>height<%else%><%if (ImgDimension(Server.MapPath("db/inventory/")&rs2("path"))) then %>width<%else%>height<%end if%><%end if%>:100px;max-width:100px;max-height:100px;" /><br/><%if (rs2("name")="") then response.write rs2("file_name") else response.write rs2("name")%><%else%><image src="file.gif" border="0"/> <%if (rs2("name")="") then response.write rs2("file_name") else response.write rs2("name")%><%end if%></a></td><%if (count mod 3)=0 then%></tr><tr><%end if
		rs2.movenext
		loop
		%></tr></table><%
	end if
	rs2.close
	query = "Select task.date_due, task.users_ID, task.task_ID, task.date_completed, task.summary, users.full_name from task INNER JOIN users ON task.users_ID = users.users_ID WHERE notes_ID="&notes_ID
	rs2.open query, conn
	if not(rs2.eof) then
		completed = not(isNull(rs2("date_completed")))
		overdue = rs2("date_due") < Date and not(completed)
		summary = rs2("summary")
		name = rs2("full_name")
		css = "task"
		if (completed) then
			datetext = "Completed: " & rs2("date_completed")
		else
			datetext = "Due: " & rs2("date_due")
		end if
		if (completed) then css = css & " completed"
		if (overdue) then css = css & " overdue"
		%><div class="<%=css%>"><span class="name"><%=name%></span>: <%=summary%><br/><%=datetext%><%
		if not (completed) and ((rs2("users_ID")=CInt(request.cookies("EPICRISE_user")) or (rs2("users_ID")=-1 and (user_rights(33))))) then
		%>
		<input type="checkbox" value="Done?" onClick="document.completetask_<%=rs2("task_ID")%>.submit();" />
		<%
		end if 
		%>
		</div>
		<%
	end if
	rs2.close
	%>
	<div id="notes<%=notes_ID%>" style="display:none;"><%=editnotetext%></div>
	<div id="noteseditform<%=notes_ID%>" style="display:none;">
	<form action="notes.asp" method="POST" name="editnoteform<%=notes_ID%>">
	<input type="hidden" name="notes_ID" value="<%=notes_ID%>" />
	<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
	<input type="hidden" name="action" value="edit" />
	<input type="hidden" name="grouptasks" value="<%if (grouptasks) then %>yes<%else%>no<%end if%>" />
	<%if (user_rights(23)) then%><table><tr><td>Hours</td><td><input type="text" name="hours" onchange="validatenumber(this);" value="<%=FormatNumber(hours, 2)%>" /></td></tr>
	<tr><td>Billable:</td><td><select name="billable">
	<%query="Select * from billable order by rank ASC"
	rs2.open query,conn
	do until rs2.eof
	%><option value="<%=rs2("billable_ID")%>"<%if (billable_ID=rs2("billable_ID")) then%> selected<%end if%>><%=rs2("name")%></option>
	<%
	rs2.movenext
	loop
	rs2.close
	%></select></td></tr>
	<tr><td>Work Date</td><td><div style="display:inline;position:relative;"><input type="text" size=8 name="date_work" value="<%=date_work%>" onfocus="calendarShow(this, <%=notes_ID%>);" readonly />&nbsp;<div id="calendar<%=notes_ID%>" name="calendar_display" style="display:none;position:absolute;top:0px;left:0px;width:250px;border: solid 1px black;background-color: white;"><%curdate=date_work%><!--#include file="calendar.asp" --></div></div></td></tr>
	</table><%
	else
	%>
	<%end if%>
	<textarea name="note" style="width:700px; height:200px;"><%=unCleanInput(editnotetext)%></textarea><br/>
	<input type="submit" value="Update" />
	</form>
	<form action="notes.asp" method="POST" name="deletenoteform<%=notes_ID%>">
	<input type="hidden" name="notes_ID" value="<%=notes_ID%>" />
	<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="grouptasks" value="<%if (grouptasks) then %>yes<%else%>no<%end if%>" />
	</form>
	</div>
	</p></td></tr></table></a>
	<%else
	query="Select * from users WHERE users_ID="&rs("task.users_ID")
	rs2.open query, conn
	if rs2.eof then
		user_name=STR_USER_NOT_FOUND
	else
		user_name=rs2("full_name")
	end if%>
	<table class="notestable" cellpadding="0" cellspacing="1"><tr><td><a href="javascript:shownote(<%=rs("task.notes_ID")%>)"><%=rs("summary")%></a> completed by <%=user_name%> on <%=formatmydatetime(LocalizeDate(rs("date_completed")))%></td></tr></table>
	<%rs2.close
	end if
	rs.movenext
loop
rs.close
%></td>
<%if (tasksexist) then
%><td valign="top" class="tasks">
<form action="dashboard.asp" method="POST" target="_BLANK">
	<input type="hidden" name="menu_visible" value="0" />
	<input type="hidden" name="jobs_visible" value="0" />
	<input type="hidden" name="settings_visible" value="0" />
	<input type="hidden" name="fullscreen" value="1" />
	<input type="hidden" name="tasklist_job_ID" value="<%=job_ID%>" />
	<input type="hidden" name="tasklist_group_tasks" value="<%if (grouptasks) then%>1<%else%>0<%end if%>" />
	<input type="Submit" value="Print Tasks" />
</form>
<form action="notes.asp" method="POST" name="grouptasks">
<input type="hidden" name="city" value="<%=job_ID%>" />
<input type="hidden" name="prevlocation" value="<%=prevlocation%>" />
<input type="hidden" name="action" value="grouptasks" />
<input type="checkbox" name="grouptasks" value="yes" <%if (grouptasks) then %>checked <%end if%> onclick="document.grouptasks.submit();"/>&nbsp;Group Tasks
</form>
<%
if (grouptasks) then

'unassigned tasks
if (job_ID>0) then
	query="Select * from task inner join notes on task.notes_ID=notes.notes_ID WHERE notes.job_ID="&job_ID&" and task.users_ID=-1 and date_completed is NULL ORDER BY task.date_due ASC"
else
	query="Select * from (task inner join notes on task.notes_ID=notes.notes_ID) inner join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and task.users_ID=-1 and date_completed is NULL ORDER BY task.date_due ASC"
end if
rs.open query, conn
if (not rs.eof) then
%><table class="tasks"><tr><td style="font-size:larger;">Unassigned Tasks</td></tr>
<%
do until rs.eof
	%><tr><td><%if (user_rights(36)) then%><div onmouseover="mouseovertask(<%=rs("task_ID")%>);" onmouseout="mouseouttask(<%=rs("task_ID")%>);"><%end if%><%=rs("date_due")%><br/><%if (user_rights(38)) then%><form action="notes.asp" method="POST" name="completetask_<%=rs("task_ID")%>" style="display:inline;"><input type="hidden" name="action" value="completetask" /><input type="hidden" name="task_ID" value="<%=rs("task_ID")%>" /><input type="hidden" name="prevlocation" value="<%=prevlocation%>" /><input type="checkbox" onclick="document.completetask_<%=rs("task_ID")%>.submit();" /></form><%end if%><a href="javascript:shownote(<%=rs("task.notes_ID")%>);"><%=rs("summary")%></a><div id="edit_task_<%=rs("task_ID")%>" style="display:none;"><input type="button" value="Edit" onclick="window.location='edittask.asp?task_ID=<%=rs("task_ID")%>';" /> <input type="button" value="Move" onclick="window.location='movetask.asp?task_ID=<%=rs("task_ID")%>';" /></div><%if (user_rights(36)) then%></div><%end if%></td></tr><tr><td>&nbsp;</td></tr><%
	rs.movenext
loop
end if
rs.close

	
if (job_ID>0) then
	if (user_rights(35)) then
		query="Select task.users_ID from (task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID WHERE notes.job_ID="&job_ID&" and date_completed is NULL and task.users_ID<>-1 GROUP BY task.users_ID"
	else
		query="Select task.users_ID from (task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID WHERE notes.job_ID="&job_ID&" and date_completed is NULL and task.users_ID="&request.cookies("EPICRISE_user")&" GROUP BY task.users_ID"
	end if
else
		if (user_rights(35)) then
		query="Select task.users_ID from ((task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID) inner join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and date_completed is NULL and task.users_ID<>-1 GROUP BY task.users_ID"
	else
		query="Select task.users_ID from ((task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID) inner join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and date_completed is NULL and task.users_ID="&request.cookies("EPICRISE_user")&" GROUP BY task.users_ID"
	end if
end if
rs2.open query, conn
formdata=""
for each item in request.form
	formdata=formdata & "&" & item & "=" & request.form(item)
next
for each item in request.querystring
	formdata=formdata & "&" & item & "=" & request.querystring(item)
next
if (Len(formdata)>0) then
	formdata= "?" & Right(formdata, Len(formdata)-1)
end if
do until rs2.eof
	query="Select * from users WHERE users_ID="&rs2("users_ID")
	rs.open query, conn
	%><table class="tasks"><tr><td style="font-size:larger;"><%=rs("full_name")%>'s Tasks</td></tr>
<%	rs.close
	if (job_ID>0) then
		query="Select * from task inner join notes on task.notes_ID=notes.notes_ID WHERE notes.job_ID="&job_ID&" and task.users_ID="&rs2("users_ID")&" and date_completed is NULL ORDER BY task.date_due ASC"
	else
		query="Select * from (task inner join notes on task.notes_ID=notes.notes_ID) inner join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and task.users_ID="&rs2("users_ID")&" and date_completed is NULL ORDER BY task.date_due ASC"
	end if
	rs.open query, conn
	do until rs.eof
		%><tr><td><%if (user_rights(36)) then%><div onmouseover="mouseovertask(<%=rs("task_ID")%>);" onmouseout="mouseouttask(<%=rs("task_ID")%>);"><%end if%><%=rs("date_due")%><br/><% if (rs("task.users_ID")=CInt(request.cookies("EPICRISE_user")) or user_rights(36)) then%><form action="notes.asp" method="POST" name="completetask_<%=rs("task_ID")%>" style="display:inline;"><input type="hidden" name="action" value="completetask" /><input type="hidden" name="task_ID" value="<%=rs("task_ID")%>" /><input type="hidden" name="prevlocation" value="<%=prevlocation%>" /><input type="checkbox" onclick="document.completetask_<%=rs("task_ID")%>.submit();" /></form><%end if%><a href="javascript:shownote(<%=rs("task.notes_ID")%>);"><%=rs("summary")%></a><div id="edit_task_<%=rs("task_ID")%>" style="display:none;"><input type="button" value="Edit" onclick="window.location='edittask.asp?task_ID=<%=rs("task_ID")%>&prevlocation=<%=Server.UrlEncode(Request.ServerVariables("URL"))%><%=Server.UrlEncode(formdata)%>';" /> <input type="button" value="Move" onclick="window.location='movetask.asp?task_ID=<%=rs("task_ID")%>';" /></div><%if (user_rights(36)) then%></div><%end if%></td></tr><tr><td>&nbsp;</td></tr><%
		rs.movenext
	loop
	rs.close
	%></table>
<%	rs2.movenext
loop
rs2.close

else 'ungrouped tasks
%><table class="tasks"><tr><td style="font-size:larger;">Job Tasks</td></tr>
<%
	if (job_ID>0) then
		query="Select * from (task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID WHERE notes.job_ID="&job_ID&" and date_completed is NULL ORDER BY task.date_due ASC"
	else
		query="Select * from ((task inner join notes on task.notes_ID=notes.notes_ID) inner join users on task.users_ID=users.users_ID) inner join job on notes.job_ID=job.job_ID WHERE job.client_ID="&(-job_ID)&" and date_completed is NULL ORDER BY task.date_due ASC"
	end if
	rs.open query, conn
	do until rs.eof
		%><tr><td><%if (user_rights(36)) then%><div onmouseover="mouseovertask(<%=rs("task_ID")%>);" onmouseout="mouseouttask(<%=rs("task_ID")%>);"><%end if%><%=rs("date_due")%><br/><% if (rs("task.users_ID")=CInt(request.cookies("EPICRISE_user"))) then%><form action="notes.asp" method="POST" name="completetask_<%=rs("task_ID")%>" style="display:inline;"><input type="hidden" name="action" value="completetask" /><input type="hidden" name="task_ID" value="<%=rs("task_ID")%>" /><input type="hidden" name="prevlocation" value="<%=prevlocation%>" /><input type="checkbox" onclick="document.completetask_<%=rs("task_ID")%>.submit();" /></form><%end if%><a href="javascript:shownote(<%=rs("task.notes_ID")%>);"><%=rs("summary")%></a> - <b><%=rs("full_name")%></b><div id="edit_task_<%=rs("task_ID")%>" style="display:none;"><input type="button" value="Edit" onclick="window.location='edittask.asp?task_ID=<%=rs("task_ID")%>';" /></div><%if (user_rights(36)) then%></div><%end if%></td></tr><tr><td>&nbsp;</td></tr><%
		rs.movenext
	loop
	rs.close
	%></table><%
end if%>
</td><%end if%></tr></table><table class="pages"><tr><td align="center">
Page<% 
if (page<>1) then
	%>&nbsp;<a href="javascript:page(1)"><%if (ordermode="ASC") then%>Oldest<%else%>Newest<%end if%></a>&nbsp;<a href="javascript:page(<%=i-1%>)"><%if (ordermode="ASC") then%>Older<%else%>Newer<%end if%></a><%
end if
for i = 1 to totalpages
	if (i=page) then
		%>&nbsp;<b><%=i%></b><%
	else
		%>&nbsp;<a href="javascript:page(<%=i%>)"><%=i%></a><%
	end if
next
if (page<>totalpages) then
	%>&nbsp;<a href="javascript:page(<%=page+1%>)"><%if (ordermode="ASC") then%>Newer<%else%>Older<%end if%></a>&nbsp;<a href="javascript:page(<%=totalpages%>)"><%if (ordermode="ASC") then%>Newest<%else%>Oldest<%end if%></a><%
end if
%></td></tr></table>
<!--<input type="button" value="Back to <%if (request.form("prevlocation")="1") then %>Call List<%else%>Pipeline<%end if%>" onclick="window.location='<%if (request.form("prevlocation")="1") then %>calllist.asp<%else%>pipeline.asp<%end if%>';" />-->
<!--#include file="bodyclose.asp" --></body></html>