<!--#include file="header.asp" -->
<!--#include file="freeaspupload.asp" -->
<% 
if (request("cust")="") or isNull(request("cust")) then
	customer_ID = 0
else
	customer_ID=CInt(request("cust"))
end if

if (customer_ID>0) then
	query="Select * from job where job_ID="&customer_ID
	rs.open query,conn
	if (rs.eof) then
		response.redirect "defaultpage.asp"
	else
		client_ID=rs("client_ID")
		job_name=rs("name")
		if (isNull(rs("name"))) then job_name=""
		rs.close
		query="Select * from client WHERE client_ID="&client_ID
		rs.open query, conn
		client_name=rs("name")
		if (rs("state")<>"") then client_name = client_name & ", " & rs("state")
	end if
	rs.close
elseif (customer_ID < 0) then
	query = "Select * from client WHERE client_ID="&(-customer_ID)
	rs.open query, conn
	client_name=rs("name")
	if (rs("state")<>"") then client_name = client_name & ", " & rs("state")
	rs.close
end if

if (request("upload")=1) then
	dim Upload
	Set Upload = New FreeASPUpload
	Upload.Save(Server.MapPath("/epicrise/db/inventory/"))
	ks=Upload.UploadedFiles.keys
	if (UBound(ks)<>-1) then
		for each filekey in ks
			path=Upload.UploadedFiles(filekey).FileName
			filename=Upload.UploadedFiles(filekey).OrigFileName
		next
	end if
	name=Upload.Form("name")
	query="Insert into `inventory` (`path`, `file_name`, `name`, `job_ID`, `users_ID`, `date_edited`) Values('"&path&"', '"&filename&"', '"&name&"', "&customer_ID&", "&Request.Cookies("EPICRISE_user")&", NOW())"
	rs.open query, conn
	response.redirect("inventory.asp?cust="&customer_ID)
end if

if (request.form("action")="delete") then
	inventory_id=request.form("inventory_ID")
	query="delete from inventory where inventory_ID="&inventory_ID
	rs.open query, conn
	response.redirect "inventory.asp?cust="&customer_ID
end if
%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title><%if (customer_ID>0) then%><%=client_name%><%=chr(32)%><%=job_name%><%else%><%=client_name%><%end if%> - Inventory</title>
<script type="text/javascript">
function disablebutton() {
document.getElementById("submitbtn").disabled=true;
document.getElementById("submitbtn").value="Uploading...";
}
function confirmform() {
return confirm("Deleting a file is permanent\nAre you sure you want to continue?");
}
</script>
<style type="text/css">
	th 
	{
		text-align: left;
	}
</style>
</head>
<body>
<!--#include file="menu.asp" --><!--#include file="bodyopen.asp" -->
<div style="font-size:18px;"><%if (customer_ID>0) then %><%=client_name%> - <%=job_name%><%else%><%=client_name%><%end if%> Inventory</div>
<div style="font-size:5px;"><br></div>
<%if (customer_ID>0) then%><form method="POST" action="inventory.asp?cust=<%=customer_ID%>&upload=1" enctype="multipart/form-data" onsubmit="disablebutton();">
<table>
<tr><td>Name:</td><td><input type="text" name="name" /></td></tr>
<tr><td>File:</td><td><input type="file" name="path" /></td></tr>
<tr><td colspan=2 align="center"><input type="submit" value="Upload" id="submitbtn" /></td></tr>
</table>
</form><%end if%>
<table><tr><th><a href="inventory.asp?cust=<%=request("cust")%>&sort=1">Name</a></th><th>Download</th><th>Uploader</th><th><a href="inventory.asp?cust=<%=request("cust")%>">Date</a></th><%if (customer_ID>0) then%><th>Edit</th><%end if%></tr>
<% set rs2=Server.CreateObject("ADODB.Recordset")
sort = 0
if (isNumeric(request("sort"))) then sort = request("sort")
if (customer_ID>0) then
	query="Select * from inventory left join notes on inventory.notes_ID=notes.notes_ID WHERE inventory.job_ID="&customer_ID&" or notes.job_ID="&customer_ID
elseif (customer_ID<0) then
	query="Select * from ((inventory left join notes on inventory.notes_ID=notes.notes_ID) left join job on notes.job_ID=job.job_ID) left join job as job2 on inventory.job_ID=job2.job_ID WHERE job.client_ID="&(-customer_ID)&" or job2.client_ID="&(-customer_ID)
else
	query="Select * from inventory left join notes on inventory.notes_ID=notes.notes_ID"
end if
if (sort=1) then 
	query = query & " ORDER BY IIF(inventory.name="""",inventory.file_name,inventory.name) ASC"
else
	query = query & " ORDER BY date_edited DESC"
end if
rs.open query, conn
do until rs.eof 
if (customer_ID>=0) then
	name=rs("name")
else
	name=rs("inventory.name")
end if
if (name="") then name=rs("file_name")
query="Select * from users WHERE `users_ID`="&rs("inventory.users_ID")
rs2.open query,conn%>
<tr><td><%=name%></td><td><a href="download.asp?file=<%=rs("inventory_ID")%>"><%=rs("file_name")%></a></td><td><%=Replace(rs2("full_name"), " ", "&nbsp;")%></td><td><%=Replace(LocalizeDate(rs("date_edited")), " ", "&nbsp;")%></td><%if (customer_ID>0) then%><td><form method="POST" action="inventory.asp?cust=<%=customer_ID%>" style="display:inline;"><input type="hidden" name="action" value="delete" /><input type="hidden" name="inventory_ID" value="<%=rs("inventory_ID")%>" /><input type="submit" value="Delete" onclick="return (confirmform());"/></form></td><%end if%></tr>
<% rs.movenext
rs2.close
loop
%>
</table>
<!--#include file="bodyclose.asp" --></body></html>