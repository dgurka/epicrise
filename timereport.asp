<!--#include file="header.asp" --><%
if not(user_rights(32)) then response.redirect "defaultpage.asp"%><!--#include file="doctype.asp" -->
<html><head>
<!--#include file="inchead.asp" -->
<title>Time Report</title>
<script type="text/javascript" src="xmlHttp.js"></script>
<script type="text/javascript">
function selectalljob()
{
	if (document.reportform.alljobselected.value=='0') {
		document.reportform.alljobselected.value='1';
		document.reportform.alljob.value='Select None';
		for (i=0; i<document.reportform.job_ID.length; i++) { 
			document.reportform.job_ID.options[i].selected = true; 
		}
		document.reportform.job_ID.focus();
	} else {
		document.reportform.alljobselected.value='0';
		document.reportform.alljob.value='Select All';
		for (i=0; i<document.reportform.job_ID.length; i++) { 
			document.reportform.job_ID.options[i].selected = false; 
		}
		document.reportform.job_ID.focus();
	}
}

function selectallusers()
{
	if (document.reportform.allusersselected.value=='0') {
		document.reportform.allusersselected.value='1';
		document.reportform.allusers.value='Select None';
		for (i=0; i<document.reportform.users_ID.length; i++) { 
			document.reportform.users_ID.options[i].selected = true; 
		}
		document.reportform.users_ID.focus();
	} else {
		document.reportform.allusersselected.value='0';
		document.reportform.allusers.value='Select All';
		for (i=0; i<document.reportform.users_ID.length; i++) { 
			document.reportform.users_ID.options[i].selected = false; 
		}
		document.reportform.users_ID.focus();
	}
}

var xmlHttp;
var calendarEditBox;
var calendarDisplayBox;

function setcalendar(calendardisplay, calendaredit)
{
	calendarEditBox=calendaredit;
	calendarDisplayBox=calendardisplay;
}

function calendarSelect(month, day, year, calendar)
{
	setcalendar(document.getElementById('calendar_'+calendar), document.getElementById('date_'+calendar));
	calendarEditBox.value=month+"/"+day+"/"+year;
	calendarMove(calendarEditBox.value, calendar);
}

function calendarUpdate()
{
if (xmlHttp.readyState==4)
{
calendarDisplayBox.innerHTML=xmlHttp.responseText;
}
}

function calendarMove(newdate, calendar)
{
setcalendar(document.getElementById('calendar_'+calendar), document.getElementById('date_'+calendar));
xmlHttp=createXmlHttp();
xmlHttp.onreadystatechange=calendarUpdate;
xmlHttp.open("GET","calendar.asp?curdate="+escape(newdate)+"&calendar_include_this="+calendar,true);
xmlHttp.send(null);

}
</script>
<style>
div.calendardiv {
	border: solid 1px black;
	width:200px;
	height: 200px;
}
table.reporttable {
background-color:#000000;
width:900px;
}
td, th {
background-color:#FFFFFF;
vertical-align:top;
}
</style>
</head><%
if (request.form("job_ID")<>"") then
	jobs=split(request.form("job_ID"), ", ")
else
	jobs=split("","")
end if
if (request.form("users_ID")<>"") then
	users=split(request.form("users_ID"), ", ")
else
	users=split("","")
end if%>
<body><!--#include file="menu.asp" -->
<!--#include file="bodyopen.asp" -->
<form action="timereport.asp" method="POST" name="reportform">
<table>
<tr><td><input type="button" name="alljob" onclick="selectalljob();" value="Select <%if (request.form("alljobselected")="1") then%>None<%else%>All<%end if%>" /><input type="hidden" name="alljobselected" value="<%if (request.form("alljobselected")<>"") then response.write request.form("alljobselected") else response.write "0"%>" /><br/>
<select name="job_ID" multiple style="width:300px;height: 200px;">
<%query="Select * from client left join job ON client.client_ID=job.client_ID where archive=0 ORDER BY client.name ASC, client.state ASC, job.name ASC"
rs.open query, conn
do until rs.eof
client_name=rs("client.name")
selected=false
for i = 0 to UBound(jobs)
	if (jobs(i)=CStr(rs("job_ID"))) then
		selected = true
		exit for
	end if
next
if not(isnull(rs("state"))) then client_name = client_name & ", " & rs("state")
job_name=rs("job.name")
%>	<option value="<%=rs("job_ID")%>"<%if (selected) then%> selected<%end if%>><%=client_name%> - <%=job_name%></option>
<% rs.movenext
loop
rs.close
%></select>
</td><td><input type="button" name="allusers" onclick="selectallusers();" value="Select <%if (request.form("allusersselected")="1") then%>None<%else%>All<%end if%>" /><input type="hidden" name="allusersselected" value="<%if (request.form("allusersselected")<>"") then response.write request.form("allusersselected") else response.write "0"%>" /><br/>
<select size="10" name="users_ID" multiple>
<%query="Select * from (`users` inner join user_level_rights on users.user_level = user_level_rights.user_level_ID) WHERE active=1 and user_rights_ID=23 ORDER BY full_name ASC"
rs.open query, conn
do until rs.eof
selected=false
for i = 0 to UBound(users)
	response.write("USER: " & users(i) & "<br/>")
	if (users(i)=CStr(rs("users_ID"))) then
		selected = true
		exit for
	end if
next
%>	<option value="<%=rs("users_ID")%>"<%if (selected) then%> selected<%end if%>><%=rs("full_name")%></option>
<% rs.movenext
loop
rs.close
%></select></td><%if (request.form("date_1")<>"") then curdate=request.form("date_1") else curdate=Date%><td>Start Date:<input type="hidden" name="date_1" id="date_1" value="<%=curdate%>" /><div class="calendardiv" id="calendar_1"><%calendar_include_this=1%><!--#include file="calendar.asp" --></div></td><%if (request.form("date_2")<>"") then curdate=request.form("date_2") else curdate=Date%><td>End Date:<input type="hidden" name="date_2" id="date_2" value="<%=curdate%>" /><br/><div class="calendardiv" id="calendar_2"><%calendar_include_this=2%><!--#include file="calendar.asp" --></div></td></tr>
<tr><td>Include Notes:&nbsp;<input type="checkbox" name="notes" value="yes" <%if (request.form("notes")="yes") then %>checked <%end if%>/></td><td colspan="3"><input type="submit" value="Update" /></td></tr></table></form>
<%if (request.form("alljobselected")<>"") then %><table cellpadding="1" cellspacing="1" class="reporttable"><tr><th>Customer</th><th>Job</th><th>Employee</th><th>Date</th><th>Hours</th><%if (request.form("notes")="yes") then%><th>Notes</th><%end if%></tr>
<%query="Select * from ((((notes) inner join users on notes.users_ID=users.users_ID) inner join job on notes.job_ID=job.job_ID) inner join client on job.client_ID=client.client_ID) WHERE (notes.date_work>=#" & request.form("date_1") & "# and notes.date_work<=#" & request.form("date_2") & "#)"
if (request.form("alljobselected")="0") then
query = query & " and (false"
for each item in jobs
	if Trim(item)<>"" then query = query & " or job.job_ID=" & item
next
query = query & ")"
end if

if (request.form("allusersselected")="0") then
query = query & " and (false"
for each item in users
	if Trim(item)<>"" then query = query & " or notes.users_ID=" & item
next
query = query & ")"
end if

query = query & " ORDER BY notes.date_work DESC"
rs.open query, conn
do until rs.eof
client_name=rs("client.name")
if not(isnull(rs("state"))) then client_name = client_name & ", " & rs("state") %>
<tr><td><%=client_name%></td><td><%=rs("job.name")%></td><td><%=rs("full_name")%></td><td><%=rs("date_work")%></td><td><%=rs("hours")%></td><%if (request.form("notes")="yes") then %><td><%=rs("note")%></td><%end if %></tr><%
rs.movenext
loop
rs.close %>
</table><br/><br/>
<table cellpadding="1" cellspacing="1" class="reporttable">
<tr><th>&nbsp;</th><% dim usertotals()
Redim usertotals(UBound(users))
for each item in users
	if Trim(item)<>"" then 
		query="Select * from users WHERE users_ID="&item
		rs.open query, conn
		response.write "<th>" & rs("full_name") & "</th>"
		rs.close
	end if
next%><th>Total</th></tr>
<%
gtotal=0
for each ljob in jobs%>
<tr><% query="Select * from job inner join client on job.client_ID=client.client_ID WHERE job.job_ID="&ljob
	rs.open query, conn
	client_name=rs("client.name")
	if not(isnull(rs("state"))) then client_name = client_name & ", " & rs("state")
	%><th><%=client_name%> - <%=rs("job.name")%></th><%
	rs.close
	jobtotal=0
	i=0
	for each luser in users
		query = "Select sum(hours) as total from notes WHERE (notes.date_work>=#" & request.form("date_1") & "# and notes.date_work<=#" & request.form("date_2") & "#) and users_ID="&luser&" and job_ID="&ljob
		rs.open query, conn
		%><td><%=rs("total")%></td><%
		if not(isNull((rs("total")))) then
			jobtotal = jobtotal + CDbl(rs("total"))
			usertotals(i)=usertotals(i)+CDbl(rs("total"))
		end if
		rs.close
		i=i+1
	next
	%><td><%=jobtotal%></td></tr><%
	gtotal = gtotal + jobtotal
next
%><th>Total</th><%
i=0
for each luser in users
	%><td><%=usertotals(i)%></td><%
	i = i + 1
next
query = "Select sum(hours) as total from notes WHERE (notes.date_work>=#" & request.form("date_1") & "# and notes.date_work<=#" & request.form("date_2") & "#)"
rs.open query, conn
%><td><%=gtotal%></td><%
rs.close%>
</table>
<%end if%>
<!--#include file="bodyclose.asp" --></body></html>